from django import forms

from core.models import Course, Classlevel, Supervisor, Module, SubModule, ModuleContent, Student, Quiz, TriviaQuiz, \
    TriviaBank, TriviaQuestion, TriviaAnswer


class CourseForm(forms.ModelForm):

    class Meta:
        model = Course
        fields = ['code','title','class_level']

        widgets = {
            'code':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'title':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'class_level':forms.Select(attrs={'class':'form-control border-primary'}),
        }


class StudentForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = ['first_name','last_name','phone','dob','parent_phone','state','current_class','teacher']

        widgets = {
            'first_name':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'last_name':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'phone':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'dob':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'parent_phone':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'state': forms.Select(attrs={'class': 'form-control border-primary'}),
            'current_class': forms.Select(attrs={'class': 'form-control border-primary'}),
            'teacher': forms.Select(attrs={'class': 'form-control border-primary'}),
        }


class ClassLevelForm(forms.ModelForm):

    class Meta:
        model = Classlevel
        fields = ['code','name']

        widgets = {
            'code':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'name':forms.TextInput(attrs={'class':'form-control border-primary'}),
        }


class SupervisorForm(forms.ModelForm):

    class Meta:
        model = Supervisor
        fields = ['phone','fullname']

        widgets = {
            'phone':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'fullname':forms.TextInput(attrs={'class':'form-control border-primary'}),
        }


class ModuleForm(forms.ModelForm):

    class Meta:
        model = Module
        fields = ['title', 'order']

        widgets = {
            'title':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'order':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'course': forms.Select(attrs={'class': 'form-control border-primary'}),
        }


class SubModuleForm(forms.ModelForm):

    class Meta:
        model = SubModule
        fields = ['title', 'order']

        widgets = {
            'title':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'order':forms.TextInput(attrs={'class':'form-control border-primary'}),
        }


class ModuleContentForm(forms.ModelForm):

    class Meta:
        model = ModuleContent
        fields = ['content', 'order']

        widgets = {
            'content':forms.Textarea(attrs={'class':'form-control border-primary'}),
            'order':forms.TextInput(attrs={'class':'form-control border-primary'}),
        }


class QuizForm(forms.ModelForm):

    class Meta:
        model = Quiz
        fields = ['name', 'module','is_roll_out']

        widgets = {
            'name':forms.Textarea(attrs={'class':'form-control border-primary'}),
            'module': forms.Select(attrs={'class': 'form-control border-primary'}),
        }


class TriviaQuizForm(forms.ModelForm):

    class Meta:
        model = TriviaQuiz
        fields = ['name',]

        widgets = {
            'name':forms.TextInput(attrs={'class':'form-control border-primary'}),
        }


class TriviaBankForm(forms.ModelForm):

    class Meta:
        model = TriviaBank
        fields = [ 'content', 'order',]

        widgets = {
            'order':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'content': forms.Textarea(attrs={'class': 'form-control border-primary'}),
            # 'category': forms.Select(attrs={'class': 'form-control border-primary'}),
        }


class TriviaQuestionForm(forms.ModelForm):

    class Meta:
        model = TriviaQuestion
        fields = [ 'label', 'order',]
        labels = {
            'label':'Question'
        }

        widgets = {
            'label':forms.TextInput(attrs={'class':'form-control border-primary'}),
            'order':forms.TextInput(attrs={'class':'form-control border-primary'}),
        }


class TriviaOptionForm(forms.ModelForm):

    class Meta:
        model = TriviaAnswer
        fields = [ 'text', 'is_correct',]
        labels = {
            'text':'Option'
        }

        widgets = {
            'text':forms.TextInput(attrs={'class':'form-control border-primary'}),
        }

