import pdb

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models.query_utils import Q
from django.forms import inlineformset_factory
from django.utils import timezone
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Permission, User
from django.http.response import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.urls.base import reverse


# todo: Add a ajax authentication to it
from django.views.generic import DeleteView

from core.models import Student, Course, Quiz, TriviaQuiz, Classlevel, Supervisor, Module, SubModule, ModuleContent, \
    TriviaBank, TriviaQuestion, TriviaAnswer
from core.utils import sendable_phone_number, send_airtime, send_sms
from frontend.forms import CourseForm, ClassLevelForm, SupervisorForm, ModuleForm, SubModuleForm, ModuleContentForm, \
    StudentForm, QuizForm, TriviaQuizForm, TriviaBankForm, TriviaQuestionForm, TriviaOptionForm


def user_login(request):
    # pdb.set_trace()
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('dashboard'))
    is_error = False
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        try:
            u = User.objects.get(Q(username=username) | Q(email=username))
        except:
            is_error = 'Invalid login credentials. Please note that the password field is case sensitive'
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            print("user logged on")
            return HttpResponseRedirect(reverse('dashboard'))
    return render(request, 'frontend/login_form.html', {'is_error': is_error})



@login_required
def dashboard(request):
    student = Student.objects.all()
    courses = Course.objects.all()
    quiz = Quiz.objects.all()
    trivia_cat = TriviaQuiz.objects.all()
    context = {
        'student': student,
        'courses': courses,
        'quiz': quiz,
        'trivia_cat': trivia_cat,

    }
    return render(request, 'frontend/dashboard.html', context)


def course_list(request, pk=None):

    if pk:
        instance = get_object_or_404(Course, pk=int(pk))
    else:
        instance = None
    form = CourseForm(instance=instance)
    if request.method == "POST":
        form = CourseForm(request.POST, instance=instance)
        if form.is_valid():
            course = form.save()
            messages.success(request, 'Operation completed successfully')
            form = CourseForm()
    courses = Course.objects.all()
    context = {
        'courses':courses,
        'form':form,
    }
    return render(request, 'frontend/course.html', context)


class CourseDeleteView(LoginRequiredMixin, DeleteView):
    # perms_to_check = ['account.admin']
    model = Course

    def get_success_url(self):
        messages.success(self.request, "Course object has been deleted successfully")
        return reverse('course_list')


def student_list(request):
    data = Student.objects.all()
    context = {
        'data':data,
    }
    return render(request, 'frontend/student_list.html', context)


def student_form(request, pk=None):
    if pk:
        instance = get_object_or_404(Student, pk=int(pk))
    else:
        instance = None
    form = StudentForm(instance=instance)
    if request.method == "POST":
        form = StudentForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save()
            messages.success(request, 'Operation completed successfully')
            form = StudentForm()
    context = {
        'form':form,
    }
    return render(request, 'frontend/add_student.html', context)



def class_level_list(request, pk=None):

    if pk:
        instance = get_object_or_404(Classlevel, pk=int(pk))
    else:
        instance = None
    form = ClassLevelForm(instance=instance)
    if request.method == "POST":
        form = ClassLevelForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save()
            messages.success(request, 'Operation completed successfully')
            form = ClassLevelForm()
    data = Classlevel.objects.all()
    context = {
        'data':data,
        'form':form,
    }
    return render(request, 'frontend/class_level.html', context)


@login_required
def send_airtime_view(request):
    resp = None
    if request.method == 'POST':
        phones = request.POST['phones']
        amount = request.POST['amount']
        if not phones or not amount:
            messages.warning(request, "Both phone and amount field must be specified")
        sendable_phones = sendable_phone_number(str(phones).split(','))
        amount = int(amount)
        resp = send_airtime(sendable_phones, amount)
        messages.success(request, "Airtime sent successfully to the entered phone numbers")
    return render(request, 'frontend/send_airtime.html', context={'resp':resp})


@login_required
def send_sms_view(request):
    resp = None
    if request.method == 'POST':
        phones = request.POST['phones']
        message = request.POST['message']
        if not phones or not message:
            messages.warning(request, "Both phone and amount field must be specified")
        sendable_phones = sendable_phone_number(str(phones).split(','))
        resp = send_sms(sendable_phones, message)
        messages.success(request, "Message sent successfully to the entered phone numbers")
    return render(request, 'frontend/send_sms.html', context={'resp':resp})


class ClassLevelDeleteView(LoginRequiredMixin, DeleteView):
    model = Course

    def get_success_url(self):
        messages.success(self.request, "object has been deleted successfully")
        return reverse('class_level_list')


def supervisor_list(request, pk=None):

    if pk:
        instance = get_object_or_404(Supervisor, pk=int(pk))
    else:
        instance = None
    form = SupervisorForm(instance=instance)
    if request.method == "POST":
        form = SupervisorForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save()
            messages.success(request, 'Operation completed successfully')
            form = SupervisorForm()
    data = Supervisor.objects.all()
    context = {
        'data':data,
        'form':form,
    }
    return render(request, 'frontend/supervisors.html', context)


def module_list(request, course_id, pk=None):
    course = get_object_or_404(Course, pk=int(course_id))
    if pk:
        instance = get_object_or_404(Module, pk=int(pk), course=course)
    else:
        instance = None
    form = ModuleForm(instance=instance)
    if request.method == "POST":
        form = ModuleForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.course = course
            obj.save()
            messages.success(request, 'Operation completed successfully')
            form = ModuleForm()
    data = Module.objects.filter(course=course)
    context = {
        'data':data,
        'form':form,
        "course":course
    }
    return render(request, 'frontend/modules.html', context)


def submodule_list(request, module_id, pk=None):
    module = get_object_or_404(Module, pk=int(module_id))
    if pk:
        instance = get_object_or_404(SubModule, pk=int(pk), module=module)
    else:
        instance = None
    form = SubModuleForm(instance=instance)
    if request.method == "POST":
        form = SubModuleForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.module = module
            obj.save()
            messages.success(request, 'Operation completed successfully')
            form = SubModuleForm()
    data = SubModule.objects.filter(module=module)
    context = {
        'data':data,
        'form':form,
        "module":module
    }
    return render(request, 'frontend/submodules.html', context)


def module_content_list(request, submodule_id, pk=None):
    submodule = get_object_or_404(SubModule, pk=int(submodule_id))
    if pk:
        instance = get_object_or_404(ModuleContent, pk=int(pk), submodule=submodule)
    else:
        instance = None
    form = ModuleContentForm(instance=instance)
    if request.method == "POST":
        form = ModuleContentForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.submodule = submodule
            obj.save()
            messages.success(request, 'Operation completed successfully')
            form = SubModuleForm()
    data = ModuleContent.objects.filter(submodule=submodule)
    context = {
        'data':data,
        'form':form,
        "submodule":submodule
    }
    return render(request, 'frontend/module_contents.html', context)


class ModuleContentDeleteView(LoginRequiredMixin, DeleteView):
    model = ModuleContent

    def get_success_url(self):
        messages.success(self.request, "Object deleted successfully")
        return reverse('module_content_list', kwargs={'submodule_id':self.kwargs['submodule_id']})


def quiz_list(request, module_id, pk=None):
    module = get_object_or_404(Module, pk=int(module_id))
    if pk:
        instance = get_object_or_404(Quiz, pk=int(pk))
    else:
        instance = None
    form = QuizForm(instance=instance)
    if request.method == "POST":
        form = QuizForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.module = module
            obj.save()
            messages.success(request, 'Operation completed successfully')
            form = QuizForm()
    data = Quiz.objects.filter(module=module)
    context = {
        'data':data,
        'form':form,
        "module":module
    }
    return render(request, 'frontend/quizzes.html', context)


def trivia_category_list(request, pk=None):
    if pk:
        instance = get_object_or_404(TriviaQuiz, pk=int(pk))
    else:
        instance = None
    form = TriviaQuizForm(instance=instance)
    if request.method == "POST":
        form = TriviaQuizForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save()
            messages.success(request, 'Operation completed successfully')
            form = TriviaQuizForm()
    data = TriviaQuiz.objects.all()
    context = {
        'data':data,
        'form':form,
    }
    return render(request, 'frontend/trivia_quizzes.html', context)


def trivia_bank_list(request, cat_id, pk=None):
    cat = get_object_or_404(TriviaQuiz, pk=int(cat_id))
    if pk:
        instance = get_object_or_404(TriviaBank, pk=int(pk))
    else:
        instance = None
    form = TriviaBankForm(instance=instance)
    if request.method == "POST":
        form = TriviaBankForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.category = cat
            obj.save()
            messages.success(request, 'Operation completed successfully')
            form = TriviaBankForm()
    data = TriviaBank.objects.filter(category=cat)
    context = {
        'data':data,
        'form':form,
        "cat":cat
    }
    return render(request, 'frontend/trivia_bank.html', context)


class TriviaBankDeleteView(LoginRequiredMixin, DeleteView):
    model = TriviaBank

    def get_success_url(self):
        messages.success(self.request, "Object deleted successfully")
        return reverse('trivia_bank_list', kwargs={'cat_id':self.kwargs['cat_id']})



@login_required
# @has_perm_access(perms_list=['account.super_admin',])
def trivia_quiz_question_list(request, cat_id, pk=None):
    triv_quiz = get_object_or_404(TriviaQuiz, pk=int(cat_id))
    if pk:
        triv_question = get_object_or_404(TriviaQuestion, pk=int(pk))
    else:
        triv_question = TriviaQuestion()
    triv_question_form = TriviaQuestionForm(instance=triv_question, prefix='triv_questiond')
    OptionFormSet = inlineformset_factory(TriviaQuestion, TriviaAnswer,
                                                      form=TriviaOptionForm, extra=1, can_delete=False)
    option_formset = OptionFormSet(instance=triv_question)

    if request.method == 'POST':
        option_formset = OptionFormSet(request.POST, instance=triv_question, prefix="triviaanswer_set")
        triv_question_form = TriviaQuestionForm(request.POST, instance=triv_question, prefix='triv_questiond')
        if triv_question_form.is_valid():
            if not pk:
                triv_question = triv_question_form.save(commit=False)
                triv_question.quiz = triv_quiz
                triv_question.save()
            else:
                triv_question = triv_question_form.save()
        print(triv_question)
        '''saving independent forms'''
        if triv_question is not None:
            option_formset.save()
            messages.success(request, "Operation completed successfully")
            return redirect(reverse('trivia_quiz_question_list', kwargs={'cat_id': int(cat_id)}))
    triv_quiz = get_object_or_404(TriviaQuiz, pk=int(cat_id))
    context = {
        'triv_question_form': triv_question_form,
        'option_formset': option_formset,
        'triv_quiz': triv_quiz,
        'is_update': pk,
    }
    return render(request, 'frontend/trivia_quiz_questions.html', context)


class TriviaQuizQuestionDeleteView(LoginRequiredMixin, DeleteView):
    model = TriviaQuestion

    def get_success_url(self):
        messages.success(self.request, "Object deleted successfully")
        return reverse('trivia_quiz_question_list', kwargs={'cat_id':self.kwargs['cat_id']})

