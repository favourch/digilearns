from django.conf.urls import include, url
from django.urls.base import reverse_lazy
from django.urls.conf import path

from frontend.views import user_login, dashboard, course_list, class_level_list, supervisor_list, module_list, \
    submodule_list, module_content_list, student_list, student_form, trivia_category_list, trivia_bank_list, \
    TriviaBankDeleteView, trivia_quiz_question_list, TriviaQuizQuestionDeleteView, ModuleContentDeleteView, \
    send_airtime_view, send_sms_view
from django.contrib.auth.views import auth_logout, logout_then_login


urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('login', user_login, name='login'),
    path('logout',logout_then_login, name='logout'),

    url(r'^courses/(?P<pk>[0-9]+)?$',course_list, name='course_list'),
    url(r'^classes/(?P<pk>[0-9]+)?$',class_level_list, name='class_level_list'),
    url(r'^airtime/send$',send_airtime_view, name='send_airtime'),
    url(r'^sms/send$',send_sms_view, name='send_sms'),
    url(r'^trivia/categories/(?P<pk>[0-9]+)?$',trivia_category_list, name='triviaquiz_list'),
    url(r'^trivia/category/(?P<cat_id>[0-9]+)/bank/(?P<pk>[0-9]+)?$',trivia_bank_list, name='trivia_bank_list'),
    url(r'^trivia/category/bank/delete/(?P<cat_id>[0-9]+)/(?P<pk>[0-9]+)?$',TriviaBankDeleteView.as_view(), name='trivia_bank_delete'),
    url(r'^trivia/quiz/question/(?P<cat_id>[0-9]+)/(?P<pk>[0-9]+)?$',trivia_quiz_question_list, name='trivia_quiz_question_list'),
    url(r'^trivia/quiz/question/delete/(?P<cat_id>[0-9]+)/(?P<pk>[0-9]+)?$',TriviaQuizQuestionDeleteView.as_view(), name='trivia_quiz_question_delete'),
    url(r'^students$',student_list, name='student_list'),
    url(r'^students/add/(?P<pk>[0-9]+)?$',student_form, name='student_form'),
    url(r'^supervisors/(?P<pk>[0-9]+)?$',supervisor_list, name='supervisor_list'),
    url(r'^modules/(?P<course_id>[0-9]+)/(?P<pk>[0-9]+)?$',module_list, name='module_list'),
    url(r'^submodules/(?P<module_id>[0-9]+)/(?P<pk>[0-9]+)?$',submodule_list, name='submodule_list'),
    url(r'^module-contents/(?P<submodule_id>[0-9]+)/(?P<pk>[0-9]+)?$',module_content_list, name='module_content_list'),
    url(r'^module-contents/delete/(?P<submodule_id>[0-9]+)/(?P<pk>[0-9]+)?$',ModuleContentDeleteView.as_view(), name='module_content_delete'),
]
