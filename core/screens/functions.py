import time

import africastalking
import requests
import schedule
import wikipedia
# Using the random module to enerate random items like the quizes
import random

# import sys
# sys.path.dirname(os.path.abspath(__file__))
# from ruamel.yaml import YAML

# from googletrans import Translator, constants

# from pprint import pprint

# from decouple import config
# from django.http import HttpResponse
# from edutech.models import (AttemptedQuestions, Classlevel, Content, Gender,
#                             Language, Question, QuizAnswers, Region, Student,
#                             Subjects, Subtopic, Topic, TriviaAnswers,
#                             TriviaQuestions, TriviaTopics, UserSessions)

from decouple import config
from django.http import HttpResponse
from edutech.models import (AttemptedQuestions, Classlevel, Content, Gender,
                            Language, Question, QuizAnswers, Region, Student,
                            Subjects, Subtopic, Topic, TriviaAnswers,
                            TriviaQuestions, TriviaTopics, UserSessions)
# from edutech.models import AttemptedQuestions
# from edutech.models import Classlevel
# from edutech.models import Content
# from edutech.models import Gender
# from edutech.models import Language
# from edutech.models import Question
# from edutech.models import QuizAnswers
# from edutech.models import Region
# from edutech.models import Student
# from edutech.models import Subjects
# from edutech.models import Subtopic
# from edutech.models import Topic
# from edutech.models import TriviaAnswers
# from edutech.models import TriviaQuestions
# from edutech.models import TriviaTopics
# from edutech.models import UserSessions


exam_type = ""

username = config('username')
api_key = config('api_key')

africastalking.initialize(username, api_key)

# Initialize a service e.g. SMS
sms = africastalking.SMS

#translator = Translator()

# translation = translator.translate("Welcome to DigiLearns. Please select your preferred language", dest="yo")
# print(f"{translation.origin} ({translation.src})")

#function to get students class
def get_all_student_class(ussd_request):
    '''
    this function gets all the student class
    '''
    student_phonenumber = ussd_request.session.get('phone_number')
    student_phonenumber = ussd_request.session.get('phone_number')
    student = Student.objects.get(studentphonenumber = student_phonenumber )
    phonenumber = student.parentphonenumber
    isClass = student.currentclass
    # number_of_questions = AttemptedQuestions.attempted_questions_by_id_number(student)
    # number_of_correct = AttemptedQuestions.get_how_many_correct_questions(student)
    # average = AttemptedQuestions.get_average_correct_questions(student)

    # message = f"DIGILEARNS PROGRESS REPORT \n {studentname} has done {number_of_questions} questions, got {number_of_correct} correct. Thank you for using DigiLearns!"
    # message = f"The student has done {number_of_questions} questions, got {number_of_correct} correct, thus their average is {average}%"
    # sms.send(message,[f'+{phonenumber}', f'+{student_phonenumber}'])
    #sms.send("Hello Message!", ["+2547xxxxxx"], callback=on_finish)

    return isClass

def get_all_student_phonenumbers(ussd_request):
    '''
    this function gets all the student phone numbeers
    '''
    student_registered = Student.objects.all()
    student_numbers = []
    for i in student_registered:
        student_numbers.append(i.studentphonenumber)
    isRegistered = "False"
    for j in student_numbers:
        if str(j) == ussd_request.session.get('phone_number'):
            isRegistered = "True"
    print('*'*30)
    print(isRegistered)     
    ussd_request.session['isregistered']= isRegistered
    return isRegistered


def make_UTME_exam_type(ussd_request):
    exam_type = 'UTME'
    return exam_type
def make_WASSCE_exam_type(ussd_request):
    exam_type = 'WASSCE'
    return exam_type
def make_POSTUTME_exam_type(ussd_request):
    exam_type = 'POST-UTME'
    return exam_type

def process_response(response_result):
    '''
    process the response from a request
    '''
    result_json = response_result.json()
    result = result_json['data']
    result_question = result['question']
    result_answer = result['answer']
    result_answers_dict = result['option'].items()
    choices = ""
    for key,value in result_answers_dict:
        individual_choice = key +" "+ value
        choices+= individual_choice + '\n'

    question_and_answer = f'{str(result_question)} \n {choices}'

    return question_and_answer,result_answer

def check_correct_answer(ussd_request):
    exam_answer = ussd_request.session.get('questionanswer')
    ussd_request.session["isCorrect"] = exam_answer
    return exam_answer



def get_english_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''

    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'english',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer
    
    return question_answer

def get_mathematics_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'mathematics',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_biology_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'biology',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_commerce_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'commerce',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_accounting_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'accounting',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_physics_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'physics',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_chemistry_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'chemistry',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_government_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'government',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_englishlit_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'englishlit',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_geography_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'geography',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_economics_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    '''
    setting up the API request
    '''
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'economics',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_history_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'history',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_civiledu_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'civiledu',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_currentaffairs_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
   
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'currentaffairs',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_insurance_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'insurance',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_crk_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'crk',"year":exam_year}
    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer

def get_irk_exam_question(ussd_request):
    ''''
    function that returns the requested question from the user
    '''
    
    url = "https://questions.aloc.ng/api/q?"
    exam_year = ussd_request.session.get('exam_year')
    querystring = {"type":exam_type,"subject":'irk',"year":exam_year}

    '''
    handling the response
    '''
    response = requests.request("GET", url, params = querystring )
    question_answer,correct_answer = process_response(response)
    ussd_request.session["correctanswer"] = correct_answer
    ussd_request.session["exam_question_answer"] = question_answer

    return question_answer


def get_classes(ussd_request):
    """
    function creates classes ditionary
    """
    classes = Classlevel.objects.all()
    user_classes = {}
    for cls in classes:
        user_classes[cls.name] = cls.name
    
    return user_classes

def get_genders(ussd_request):
    """
    function creates gender dictionary
    """
    genders = Gender.objects.all()
    user_genders = {}
    for gender in genders:
        user_genders[gender.name] = gender.name
    
    return user_genders

def get_regions(ussd_request):
    """
    function creates regions dictionary
    """
    regions = Region.objects.all()
    user_regions = {}
    for region in regions:
        user_regions[region.name] = region.name
    
    return user_regions

def update_screens(ussd_request):
    """
    function gets and saves users' ussd interaction data
    """
    screens = ussd_request.session.get('ussd_interaction')
    session_id = ussd_request.session_id
    phone_number = ussd_request.phone_number
    sess , created = UserSessions.objects.get_or_create(sessionid=session_id, phonenumber=phone_number, screens=screens)

    if not created:
        sess.screens = screens
        sess.save()


def save_student(ussd_request):
    """
    function saves the details entered by a user
    """
    student_phonenumber = ussd_request.session.get('phone_number', '2348037834276')
    full_name = ussd_request.session.get('student_name', 'Favour')
    age = ussd_request.session.get('age', 26)
    # parent_phonenumber1 = ussd_request.session.get('parent_phone', '2348037834276')
    # country_code = 234
    parent_phonenumber = ussd_request.session.get(str(234) + 'parent_phone', '2348037834276')
    location = ussd_request.session.get('location', 'Home')
    gender = ussd_request.session.get('gender', 'female')
    current_class = ussd_request.session.get('class', 'SS2')
    print(f'{current_class} what you are looking for')
    print(ussd_request.session.get('ussd_interaction'))

    student , _ = Student.objects.get_or_create(studentphonenumber=student_phonenumber, fullname=full_name, age=age, parentphonenumber=parent_phonenumber, location=location, gender=gender, currentclass=current_class)

    ussd_request.session['new_name']= student.fullname
    ussd_request.session['new_class']= student.currentclass
    ussd_request.session['new_location'] = student.location
    ussd_request.session['new_student_number'] = student.studentphonenumber

    return student.fullname 

def get_subtopic(ussd_request):
    """
    function retrieves saved solar components content
    """
    subtopic , _ = Subtopic.objects.get_or_create(subtopicname="Identify components of the Solar System")
    content , _ = Content.objects.get_or_create(subtopic_id=subtopic.id)
    ussd_request.session["components_content"] = content.contenttext['components']
    return content.contenttext

def get_earth_relation(ussd_request):
    """
    function retrieves earth's relation subtopics' content
    """
    subtopic , _ = Subtopic.objects.get_or_create(subtopicname="Earth's position to the sun and other planets")
    content , _ = Content.objects.get_or_create(subtopic_id=subtopic.id)
    ussd_request.session["relation_content"] = content.contenttext['relation']
    return content.contenttext

def get_earths_shape(ussd_request):
    """
    function returns proof of earth's shape content
    """
    subtopic , _ = Subtopic.objects.get_or_create(subtopicname="Proof of the earth's shape")
    content , _ = Content.objects.get_or_create(subtopic_id=subtopic.id)
    ussd_request.session["earths_shape"] = content.contenttext['proof']
    return content.contenttext

def get_correct_answer(ussd_request):
    '''
    get correct answer
    '''
    question= Question.objects.get(questiontext="How many planets are in the solar system")
    answer = QuizAnswers.objects.get(question = question.id)

    ussd_request.session["correct_answer"] = answer.answer
    
    return answer

def add_attempted_quiz_correct(ussd_request):
    '''
    adds a correct question to attempted quiz
    '''
    question= Question.objects.get(questiontext="How many planets are in the solar system")
    student_phonenumber = ussd_request.session.get('phone_number')
    student = Student.objects.get(studentphonenumber = student_phonenumber )
    attemptedquestion = AttemptedQuestions.objects.create(student= student, quizquestions= question, isCorrect = True)

    return attemptedquestion

def add_attempted_quiz_incorrect(ussd_request):
    '''
    adds an incorrect question to attempted quiz
    '''
    question= Question.objects.get(questiontext="How many planets are in the solar system")
    student_phonenumber = ussd_request.session.get('phone_number')
    student = Student.objects.get(studentphonenumber = student_phonenumber )
    attemptedquestion = AttemptedQuestions.objects.create(student= student, quizquestions= question, isCorrect = False)

    return attemptedquestion

def get_topical_quiz(ussd_request):
    """
    function gets solar size question
    """
    question , _ = Question.objects.get_or_create(questiontext="How many planets are in the solar system")
    ussd_request.session["planet_quiz"] = question.questiontext
    
    just_question = question.questiontext
    question_choices = question.questionchoices

    choices_list = question_choices.split(",")
    spaced_choices = {}
    for i in choices_list:
        spaced_choices[i] = i
    
    ussd_request.session["question_answer"] = just_question
    ussd_request.session["spaced_choices"] = spaced_choices

    return just_question

#Function to send parent students report
def send_parent_report(ussd_request):
    '''
    function that sends the parent a message on the student's progress
    '''

    # Initialize the Africastalking SDK
    # username = config('username')
    # api_key = config('api_key')

    # username = "sandbox"    
    # api_key = "288f0e4d89b7f66eeef81bf989ce9ba7fea467747fab5a176f15f488439f147f"  
    #sender_id = "774"

    
    username = "digilearn"    
    api_key = "4b0f4763cc0996c9fe1ce532818c5f9c639be4557ad111d0815117200748def1" 

    africastalking.initialize(username, api_key)

    # Initialize a service e.g. SMS
    sms = africastalking.SMS
    
    student_phonenumber = ussd_request.session.get('phone_number')
    student = Student.objects.get(studentphonenumber = student_phonenumber )
    phonenumber = student.parentphonenumber
    studentname = student.fullname
    number_of_questions = AttemptedQuestions.attempted_questions_by_id_number(student)
    number_of_correct = AttemptedQuestions.get_how_many_correct_questions(student)
    # average = AttemptedQuestions.get_average_correct_questions(student)

    message = f"DIGILEARNS PROGRESS REPORT \n {studentname} has done {number_of_questions} questions, got {number_of_correct} correct. Thank you for using DigiLearns!"
    # message = f"The student has done {number_of_questions} questions, got {number_of_correct} correct, thus their average is {average}%"
    sms.send(message,[f'+{phonenumber}', f'+{student_phonenumber}'])
    #sms.send("Hello Message!", ["+2547xxxxxx"], callback=on_finish) 

    return "sent"


def wikisearch(ussd_request):
    '''
    function allows user to initiate a search
    '''
    searchtearm = ussd_request.session.get('search_term')
    response = wikipedia.summary(searchtearm,sentences = 1)

    ussd_request.session["wikiresult"] = response
    
    return response

def send_subtopic_data(ussd_request):
    """
    function retrieves saved solar components content
    """
    #subtopic , _ = Subtopic.objects.get_or_create(subtopicname="Identify components of the Solar System")
    subtopic , _ = Subtopic.objects.get_or_create(subtopicname="Identify components of the Solar System")
    content , _ = Content.objects.get_or_create(subtopic_id=subtopic.id)
    student_phonenumber = ussd_request.session.get('phone_number')
    student = Student.objects.get(studentphonenumber = student_phonenumber )

    message = f"Subtopic: Identify components of the Solar System \n  {content.contenttext}"
    #message = f"Subtopic: Identify components of the Solar System \n  {content.contenttext}"
    sms.send(message,[f'+{student.studentphonenumber}'])
    
    return message

def process_choices(multiples):
    '''
    process the choices into a single string with new lines
    '''
    choices_list = multiples.split(",")
    spaced_choices = {}
    for i in choices_list:
        spaced_choices[i] = i
    
    return spaced_choices

def get_music_question(ussd_request):
    '''
    this functions retrieves a question from the database
    '''
    musicTopic = TriviaTopics.objects.get(name = 'Music')
    # random.choice() here is used to generate random quiz from the db
    music_question = random.choice(TriviaQuestions.objects.get(triviatopic = musicTopic))
    music_choices = process_choices(music_question.questionanswers)
    
    question_answer = f"{music_question.questiontext}"

    ussd_request.session["music_question"] = question_answer
    ussd_request.session["music_answers"] = music_choices

    return question_answer

def get_literature_question(ussd_request):
    '''
    this functions retrieves a question from the database
    '''
    litTopic = TriviaTopics.objects.get(name = 'African Literature')
    lit_question = TriviaQuestions.objects.get(triviatopic = litTopic)
    lit_choices = process_choices(lit_question.questionanswers)
    
    question_answer = f"{lit_question.questiontext}"

    ussd_request.session["lit_question"] = question_answer
    ussd_request.session["lit_answers"] = lit_choices

    return question_answer

def music_correct(ussd_request):
    '''
    gets the correct answer for the music question
    '''
    musicTopic = TriviaTopics.objects.get(name = 'Music')
    music_question = TriviaQuestions.objects.get(triviatopic = musicTopic)
    music_answer = TriviaAnswers.objects.get(triviaquestion = music_question)

    ussd_request.session["music_correct_answer"] = music_answer

    return music_answer

def lit_correct(ussd_request):
    '''
    gets the correct answer for the music question
    '''
    litTopic = TriviaTopics.objects.get(name = 'African Literature')
    lit_question = TriviaQuestions.objects.get(triviatopic = litTopic)
    lit_answer = TriviaAnswers.objects.get(triviaquestion = lit_question.id)

    ussd_request.session["lit_correct_answer"] = lit_answer

    return lit_answer