import json
from encodings import utf_8

import africastalking
from decouple import config
from datetime import datetime, timedelta
import random
from django.db.models import Sum
import requests

from core.models import Student, UserSession, Language, Classlevel, State, CourseProgress, Course, Module, \
    ModuleContent, Quiz, SubModule, QuizProgress, Question, Answer, QuizTakerAnswer, TriviaQuiz, TriviaQuizProgress, \
    TriviaQuestion, TriviaAnswer, TriviaQuizTakerAnswer, TriviaQuizResult, GlobalConfig, PrepExamProgress, \
    StudentQuestion, Supervisor, TriviaBank, TriviaBankProgress
from core.serializers import StudentSerializer
from core.utils import get_student_configuration, get_student, search_word, TextUtil, get_super_teacher, \
    generate_digits, send_sms, get_clobal_config
from digilearns.settings import PREP_EXAM_QUESTIONS

username = config('AT_USERNAME')
api_key = config('AT_API_KEY')

africastalking.initialize(username, api_key)

# Initialize a service e.g. SMS
sms = africastalking.SMS


def is_student_registered(ussd_request):
    phone = ussd_request.session.get('phone_number')
    intr = ussd_request.session.get('ussd_interaction')
    if Student.objects.filter(phone=phone).exists():
        return "True"
    elif Supervisor.objects.filter(phone=phone).exists():
        return "Admin"
    else:
        return "False"


def update_interaction_session(ussd_request):
    """
    function get or create ussd interaction data session
    """
    print("geld")
    screens = ussd_request.session.get('ussd_interaction')
    session_id = ussd_request.session_id
    phone_number = ussd_request.phone_number
    sess, _ = UserSession.objects.get_or_create(session_id=session_id, phone=phone_number)
    sess.screens = screens
    sess.save()


def get_all_languages(ussd_request):
    languages = Language.objects.all()
    # serializer = LanguageSerializer(languages, many=True)
    # print(serializer.data)
    return languages


def set_language(ussd_session):
    language = ussd_session.session.get('chosen_language')
    phone = ussd_session.session.get('phone_number')
    # ussd_session.session['default_language'] = language
    print("first")
    stud_conf = get_student_configuration(phone)
    print(stud_conf)
    stud_conf.default_language = Language.objects.get(code=language)
    stud_conf.save()
    print(ussd_session.session.get('user_screens'))
    print("language is ", language)
    return language


def save_student(ussd_request):
    """
    function saves the details entered by a user
    """
    student_phone = ussd_request.session.get('phone_number')
    first_name = str(ussd_request.session.get('first_name', "")).title()
    last_name = str(ussd_request.session.get('last_name', "")).title()
    dob = ussd_request.session.get('dob', "")
    parent_phone = str(ussd_request.session.get('parent_phone', ""))
    parent_phone = "234" + str(parent_phone[1:])
    location = ussd_request.session.get('location', '')
    current_class = ussd_request.session.get('class', '')

    print("first_name ", first_name)
    print("last_name ", last_name)
    print(f'{current_class} what you are looking for')
    # print(ussd_request.session.get('ussd_interaction'))
    new_dob = datetime.strptime(dob, '%d/%m/%Y')
    stud_class = Classlevel.objects.get(code=current_class)
    stud_state = State.objects.get(name=location)
    print(new_dob)
    try:
        student = Student.objects.get(phone=student_phone)
    except:
        student = Student.objects.create(phone=student_phone, first_name=first_name, dob=new_dob, last_name=last_name,
                                         parent_phone=parent_phone, state=stud_state, current_class=stud_class)

    return student.fullname


def get_all_classes(ussd_session):
    return Classlevel.objects.all()


def get_all_locations(ussd_session):
    return State.objects.all()


def get_student_data(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    return student


def get_student_course_progress1(ussd_request):
    phone = ussd_request.session.get('phone_number')
    course = Course.objects.get(code=ussd_request.session.get('course_to_learn'))
    student = get_student(phone)
    last_progress = CourseProgress.objects.filter(student=student, course=course).order_by('created').last()

    if last_progress:
        last_module = last_progress.module
        last_submodule = last_progress.submodule
        last_content = last_progress.module_content
        print("last content is ", last_content.order)
        if last_progress.is_module_completed:
            if last_progress.is_submodule_completed:
                if last_progress.is_quiz_completed:
                    # get next content
                    if ModuleContent.objects.filter(submodule=last_submodule, order=last_content.order+1).exists():
                        next_content = ModuleContent.objects.get(submodule=last_submodule, order=last_content.order+1)

                        output = {
                            'type': 'content',
                            "module": last_module.order,
                            "submodule": next_content.submodule.order,
                            "content": next_content.order,
                            "course": course.code,
                        }
                    else:
                        """get next submodule"""
                        last_progress.is_submodule_completed = True
                        if SubModule.objects.filter(module=last_module, order=last_module.order + 1).exists():
                            next_submodule = SubModule.objects.get(module=last_module, order=last_module.order + 1)
                            if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                                next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                                output = {
                                    'type': 'content',
                                    "module": last_module.order,
                                    "submodule": next_submodule.order,
                                    "content": next_content.order,
                                    "course": course.code,
                                }
                            else:
                                next_content = None
                                output = {
                                    'type': 'done',
                                    "course": course.code,
                                }
                        else:
                            """get next module"""
                            last_progress.is_module_completed = True
                            if Module.objects.filter(course=course,
                                                        order=last_module.order + 1).exists():
                                next_module = Module.objects.get(course=course,
                                                                       order=last_module.order + 1)
                                submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                                if ModuleContent.objects.filter(submodule=submodule).order_by('order').first():
                                    next_content = ModuleContent.objects.filter(submodule=submodule).order_by(
                                        'order').first()
                                    output = {
                                        'type': 'content',
                                        "module": next_module.order,
                                        "submodule": next_content.submodule.order,
                                        "content": next_content.order,
                                        "course": course.code,
                                    }
                                else:
                                    next_content = None
                                    output = {
                                        'type': 'done',
                                        "course": course.code,
                                    }
                            else:
                                next_content = None
                                output = {
                                    'type': 'done',
                                    "course": course.code,
                                }
                elif Quiz.objects.filter(module=last_module).exists():
                    output = {
                        'type':'quiz',
                        "module":last_module.order,
                        "course":course.code,
                    }
                else:
                    last_progress.is_quiz_completed = True
                    """get next submodule"""
                    if SubModule.objects.filter(module=last_module, order=last_module.order + 1).exists():
                        next_submodule = SubModule.objects.get(module=last_module,
                                                               order=last_module.order + 1)
                        if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                            next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by(
                                'order').first()
                            output = {
                                'type': 'content',
                                "module": last_module.order,
                                "submodule": next_content.submodule.order,
                                "content": next_content.order,
                                "course": course.code,
                            }
                        else:
                            next_content = None
                            output = {
                                'type': 'done',
                                "course": course.code,
                            }
                    else:
                        """get next module"""
                        last_progress.is_module_completed = True
                        if Module.objects.filter(course=course,
                                                 order=last_module.order + 1).exists():
                            next_module = Module.objects.get(course=course,
                                                             order=last_module.order + 1)
                            submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                            if ModuleContent.objects.filter(submodule=submodule).order_by(
                                    'order').first():
                                next_content = ModuleContent.objects.filter(submodule=submodule).order_by(
                                    'order').first()
                                output = {
                                    'type': 'content',
                                    "module": next_module.order,
                                    "submodule": next_content.submodule.order,
                                    "content": next_content.order,
                                    "course": course.code,
                                }
                            else:
                                next_content = None
                                output = {
                                    'type': 'done',
                                    "course": course.code,
                                }
                        else:
                            next_content = None
                            output = {
                                'type': 'done',
                                "course": course.code,
                            }

            elif ModuleContent.objects.filter(submodule=last_submodule, order=last_content.order + 1).exists():
                next_content = ModuleContent.objects.get(submodule=last_submodule, order=last_content.order + 1)
                output = {
                    'type': 'content',
                    "module": last_module.order,
                    "submodule": next_content.submodule.order,
                    "content": next_content.order,
                    "course": course.code,
                }
            else:
                last_progress.is_submodule_completed = True
                """get next submodule"""
                if SubModule.objects.filter(module=last_module, order=last_module.order + 1).exists():
                    next_submodule = SubModule.objects.get(module=last_module,
                                                           order=last_module.order + 1)
                    if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                        next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                        output = {
                            'type': 'content',
                            "module": last_module.order,
                            "submodule": next_content.submodule.order,
                            "content": next_content.order,
                            "course": course.code,
                        }
                    else:
                        next_content = None
                        output = {
                            'type': 'done',
                            "course": course.code,
                        }
                else:
                    """get next module"""
                    last_progress.is_module_completed = True
                    if Module.objects.filter(course=course,
                                             order=last_module.order + 1).exists():
                        next_module = Module.objects.get(course=course,
                                                         order=last_module.order + 1)
                        submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                        if ModuleContent.objects.filter(submodule=submodule).order_by(
                                'order').first():
                            next_content = ModuleContent.objects.filter(submodule=submodule).order_by(
                                'order').first()
                            output = {
                                'type': 'content',
                                "module": next_module.order,
                                "submodule": next_content.submodule.order,
                                "content": next_content.order,
                                "course": course.code,
                            }
                        else:
                            next_content = None
                            output = {
                                'type': 'done',
                                "course": course.code,
                            }
                    else:
                        next_content = None
                        output = {
                            'type': 'done',
                            "course": course.code,
                        }
        elif ModuleContent.objects.filter(submodule=last_submodule, order=last_content.order + 1).exists():
            next_content = ModuleContent.objects.get(submodule=last_submodule, order=last_content.order + 1)
            output = {
                'type': 'content',
                "module": last_module.order,
                "submodule": next_content.submodule.order,
                "content": next_content.order,
                "course": course.code,
            }
        else:
            last_progress.is_module_completed = True
            """get next module"""
            if Module.objects.filter(course=course, order=last_module.order + 1).exists():
                next_module = Module.objects.get(course=course, order=last_module.order + 1)
                if SubModule.objects.filter(module=next_module).order_by('order').first():
                    next_submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                    if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                        next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                        output = {
                            'type': 'content',
                            "module": next_module.order,
                            "submodule": next_content.submodule.order,
                            "content": next_content.order,
                            "course": course.code,
                        }
                    else:
                        next_content = None
                        output = {
                            'type': 'done',
                            "course": course.code,
                        }
                else:
                    next_content = None
                    output = {
                        'type': 'done',
                        "course": course.code,
                    }


            else:
                next_content = None
                output = {
                    'type': 'done',
                    "course": course.code,
                }
    else:
        """get first module"""
        if Module.objects.filter(course=course).order_by('order').first():
            next_module = Module.objects.filter(course=course).order_by('order').first()
            if SubModule.objects.filter(module=next_module).order_by('order').first():
                next_submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                    next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                    output = {
                        'type': 'content',
                        "module": next_module.order,
                        "submodule": next_content.submodule.order,
                        "content": next_content.order,
                        "course": course.code,
                    }
                else:
                    next_content = None
                    output = {
                        'type': 'done',
                        "course": course.code,
                    }
            else:
                next_content = None
                output = {
                    'type': 'done',
                    "course": course.code,
                }

        else:
            output = {
                'type': 'no_data',
                "course": course.code,
            }
    print("Output is ", output)
    if last_progress:
        last_progress.save()
    return output


def get_student_course_progress2(ussd_request):
    phone = ussd_request.session.get('phone_number')
    course = Course.objects.get(code=ussd_request.session.get('course_to_learn'))
    student = get_student(phone)
    last_progress = CourseProgress.objects.filter(student=student, course=course).order_by('created').last()

    if last_progress:

        last_content = last_progress.module_content
        last_submodule = last_content.submodule
        last_module = last_submodule.module

        if last_content:
            if ModuleContent.objects.filter(submodule=last_submodule, order=last_content.order + 1).exists():
                next_content = ModuleContent.objects.get(submodule=last_submodule, order=last_content.order + 1)

                output = {
                    'type': 'content',
                    "module": last_module.order,
                    "submodule": next_content.submodule.order,
                    "content": next_content.order,
                    "course": course.code,
                }
            else:
                """get next submodule"""
                if SubModule.objects.filter(module=last_module, order=last_submodule.order + 1).exists():
                    next_submodule = SubModule.objects.get(module=last_module, order=last_submodule.order + 1)
                    if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                        next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                        output = {
                            'type': 'content',
                            "module": last_module.order,
                            "submodule": next_submodule.order,
                            "content": next_content.order,
                            "course": course.code,
                        }
                    else:
                        output = {
                            'type': 'done',
                            "course": course.code,
                        }
                else:
                    """get next module"""
                    if Module.objects.filter(course=course, order=last_module.order + 1).exists():
                        next_module = Module.objects.get(course=course, order=last_module.order + 1)
                        submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                        if submodule:
                            if ModuleContent.objects.filter(submodule=submodule).order_by('order').first():
                                next_content = ModuleContent.objects.filter(submodule=submodule).order_by(
                                    'order').first()
                                output = {
                                    'type': 'content',
                                    "module": next_module.order,
                                    "submodule": next_content.submodule.order,
                                    "content": next_content.order,
                                    "course": course.code,
                                }
                            else:
                                output = {
                                    'type': 'done',
                                    "course": course.code,
                                }
                        else:
                            output = {
                                'type': 'done',
                                "course": course.code,
                            }
                    else:
                        output = {
                            'type': 'done',
                            "course": course.code,
                        }

        if last_progress.is_module_completed:
            if last_progress.is_submodule_completed:
                if last_progress.is_quiz_completed:
                    # get next content
                    if ModuleContent.objects.filter(submodule=last_submodule, order=last_content.order + 1).exists():
                        next_content = ModuleContent.objects.get(submodule=last_submodule, order=last_content.order + 1)

                        output = {
                            'type': 'content',
                            "module": last_module.order,
                            "submodule": next_content.submodule.order,
                            "content": next_content.order,
                            "course": course.code,
                        }
                    else:
                        """get next submodule"""
                        if SubModule.objects.filter(module=last_module, order=last_submodule.order + 1).exists():
                            next_submodule = SubModule.objects.get(module=last_module, order=last_submodule.order + 1)
                            if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                                next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by(
                                    'order').first()
                                output = {
                                    'type': 'content',
                                    "module": last_module.order,
                                    "submodule": next_submodule.order,
                                    "content": next_content.order,
                                    "course": course.code,
                                }
                            else:
                                next_content = None
                                output = {
                                    'type': 'done',
                                    "course": course.code,
                                }
                        else:
                            """get next module"""
                            if Module.objects.filter(course=course, order=last_module.order + 1).exists():
                                next_module = Module.objects.get(course=course, order=last_module.order + 1)
                                submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                                if submodule:
                                    if ModuleContent.objects.filter(submodule=submodule).order_by('order').first():
                                        next_content = ModuleContent.objects.filter(submodule=submodule).order_by(
                                            'order').first()
                                        output = {
                                            'type': 'content',
                                            "module": next_module.order,
                                            "submodule": next_content.submodule.order,
                                            "content": next_content.order,
                                            "course": course.code,
                                        }
                                    else:
                                        output = {
                                            'type': 'done',
                                            "course": course.code,
                                        }
                                else:
                                    output = {
                                        'type': 'done',
                                        "course": course.code,
                                    }
                            else:
                                output = {
                                    'type': 'done',
                                    "course": course.code,
                                }

                elif Quiz.objects.filter(module=last_module).exists():
                    output = {
                        'type':'quiz',
                        "module":last_module.order,
                        "course":course.code,
                    }
                else:
                    last_progress.is_quiz_completed = True
                    """get next submodule"""
                    if SubModule.objects.filter(module=last_module, order=last_submodule.order + 1).exists():
                        next_submodule = SubModule.objects.get(module=last_module,
                                                               order=last_submodule.order + 1)
                        if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                            next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by(
                                'order').first()
                            output = {
                                'type': 'content',
                                "module": last_module.order,
                                "submodule": next_content.submodule.order,
                                "content": next_content.order,
                                "course": course.code,
                            }
                        else:
                            output = {
                                'type': 'done',
                                "course": course.code,
                            }
                    else:
                        """get next module"""
                        last_progress.is_module_completed = True
                        if Module.objects.filter(course=course,
                                                 order=last_module.order + 1).exists():
                            next_module = Module.objects.get(course=course,
                                                             order=last_module.order + 1)
                            submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                            if ModuleContent.objects.filter(submodule=submodule).order_by(
                                    'order').first():
                                next_content = ModuleContent.objects.filter(submodule=submodule).order_by(
                                    'order').first()
                                output = {
                                    'type': 'content',
                                    "module": next_module.order,
                                    "submodule": next_content.submodule.order,
                                    "content": next_content.order,
                                    "course": course.code,
                                }
                            else:
                                output = {
                                    'type': 'done',
                                    "course": course.code,
                                }
                        else:
                            output = {
                                'type': 'done',
                                "course": course.code,
                            }

            elif ModuleContent.objects.filter(submodule=last_submodule, order=last_content.order + 1).exists():
                next_content = ModuleContent.objects.get(submodule=last_submodule, order=last_content.order + 1)
                output = {
                    'type': 'content',
                    "module": last_module.order,
                    "submodule": next_content.submodule.order,
                    "content": next_content.order,
                    "course": course.code,
                }
            else:
                last_progress.is_submodule_completed = True
                """get next submodule"""
                if SubModule.objects.filter(module=last_module, order=last_module.order + 1).exists():
                    next_submodule = SubModule.objects.get(module=last_module,
                                                           order=last_module.order + 1)
                    if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                        next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                        output = {
                            'type': 'content',
                            "module": last_module.order,
                            "submodule": next_content.submodule.order,
                            "content": next_content.order,
                            "course": course.code,
                        }
                    else:
                        next_content = None
                        output = {
                            'type': 'done',
                            "course": course.code,
                        }
                else:
                    """get next module"""
                    last_progress.is_module_completed = True
                    if Module.objects.filter(course=course,
                                             order=last_module.order + 1).exists():
                        next_module = Module.objects.get(course=course,
                                                         order=last_module.order + 1)
                        submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                        if ModuleContent.objects.filter(submodule=submodule).order_by(
                                'order').first():
                            next_content = ModuleContent.objects.filter(submodule=submodule).order_by(
                                'order').first()
                            output = {
                                'type': 'content',
                                "module": next_module.order,
                                "submodule": next_content.submodule.order,
                                "content": next_content.order,
                                "course": course.code,
                            }
                        else:
                            next_content = None
                            output = {
                                'type': 'done',
                                "course": course.code,
                            }
                    else:
                        next_content = None
                        output = {
                            'type': 'done',
                            "course": course.code,
                        }
        elif ModuleContent.objects.filter(submodule=last_submodule, order=last_content.order + 1).exists():
            next_content = ModuleContent.objects.get(submodule=last_submodule, order=last_content.order + 1)
            output = {
                'type': 'content',
                "module": last_module.order,
                "submodule": next_content.submodule.order,
                "content": next_content.order,
                "course": course.code,
            }
        else:
            last_progress.is_module_completed = True
            """get next module"""
            if Module.objects.filter(course=course, order=last_module.order + 1).exists():
                next_module = Module.objects.get(course=course, order=last_module.order + 1)
                if SubModule.objects.filter(module=next_module).order_by('order').first():
                    next_submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                    if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                        next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                        output = {
                            'type': 'content',
                            "module": next_module.order,
                            "submodule": next_content.submodule.order,
                            "content": next_content.order,
                            "course": course.code,
                        }
                    else:
                        next_content = None
                        output = {
                            'type': 'done',
                            "course": course.code,
                        }
                else:
                    next_content = None
                    output = {
                        'type': 'done',
                        "course": course.code,
                    }


            else:
                next_content = None
                output = {
                    'type': 'done',
                    "course": course.code,
                }
    else:
        """get first module"""
        if Module.objects.filter(course=course).order_by('order').first():
            next_module = Module.objects.filter(course=course).order_by('order').first()
            if SubModule.objects.filter(module=next_module).order_by('order').first():
                next_submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                    next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                    output = {
                        'type': 'content',
                        "module": next_module.order,
                        "submodule": next_content.submodule.order,
                        "content": next_content.order,
                        "course": course.code,
                    }
                else:
                    next_content = None
                    output = {
                        'type': 'done',
                        "course": course.code,
                    }
            else:
                next_content = None
                output = {
                    'type': 'done',
                    "course": course.code,
                }

        else:
            output = {
                'type': 'no_data',
                "course": course.code,
            }
    print("Output is ", output)
    if last_progress:
        last_progress.save()
    return output


def get_student_course_progress(ussd_request):
    phone = ussd_request.session.get('phone_number')
    course = Course.objects.get(code=ussd_request.session.get('course_to_learn'))
    student = get_student(phone)
    last_progress = CourseProgress.objects.filter(student=student, course=course).order_by('created').last()

    if last_progress:

        last_content = last_progress.module_content
        last_submodule = last_content.submodule
        last_module = last_submodule.module

        if ModuleContent.objects.filter(submodule=last_submodule, order=last_content.order + 1).exists():
            next_content = ModuleContent.objects.get(submodule=last_submodule, order=last_content.order + 1)

            output = {
                'type': 'content',
                "module": last_module.order,
                "submodule": next_content.submodule.order,
                "content": next_content.order,
                "course": course.code,
            }
        else:
            output = {
                'type': 'go_next',
                "module": last_module.order,
                "submodule": last_submodule.order,
                "course": course.code,
            }

    else:
        """get first module"""
        if Module.objects.filter(course=course).order_by('order').first():
            next_module = Module.objects.filter(course=course).order_by('order').first()
            if SubModule.objects.filter(module=next_module).order_by('order').first():
                next_submodule = SubModule.objects.filter(module=next_module).order_by('order').first()
                if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                    next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                    output = {
                        'type': 'content',
                        "module": next_module.order,
                        "submodule": next_content.submodule.order,
                        "content": next_content.order,
                        "course": course.code,
                    }
                else:
                    output = {
                        'type': 'done',
                        "course": course.code,
                    }
            else:
                output = {
                    'type': 'done',
                    "course": course.code,
                }

        else:
            output = {
                'type': 'no_data',
                "course": course.code,
            }
    print("Output is ", output)
    if last_progress:
        last_progress.save()
    return output

def get_next_submodule_content(ussd_request):
    phone = ussd_request.session.get('phone_number')
    course = Course.objects.get(code=ussd_request.session.get('course_to_learn'))
    student = get_student(phone)
    last_progress = CourseProgress.objects.filter(student=student, course=course).order_by('created').last()

    last_content = last_progress.module_content
    last_submodule = last_content.submodule
    last_module = last_submodule.module

    last_progress.is_submodule_completed = True
    if SubModule.objects.filter(module=last_module, order=last_submodule.order + 1).exists():
        next_submodule = SubModule.objects.get(module=last_module, order=last_submodule.order + 1)
        if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
            next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
            output = {
                'type': 'content',
                "module": last_module.order,
                "submodule": next_submodule.order,
                "content": next_content.order,
                "course": course.code,
            }
        else:
            next_content = None
            output = {
                'type': 'done',
                "course": course.code,
            }
    else:
        """get next module"""
        output = {
            'type': 'go_next_module',
            "module": last_module.order,
            "submodule": last_submodule.order,
            "course": course.code,
        }

    print("Output is ", output)
    if last_progress:
        last_progress.save()
    return output


def get_next_module_content(ussd_request):
    phone = ussd_request.session.get('phone_number')
    course = Course.objects.get(code=ussd_request.session.get('course_to_learn'))
    student = get_student(phone)
    last_progress = CourseProgress.objects.filter(student=student, course=course).order_by('created').last()
    print("asassas")
    last_content = last_progress.module_content
    last_submodule = last_content.submodule
    last_module = last_submodule.module
    print("dsdsdsds")
    last_progress.is_module_completed = True
    if Module.objects.filter(course=course, order=last_module.order + 1).exists():
        next_module = Module.objects.get(course=course, order=last_module.order + 1)
        if SubModule.objects.filter(module=next_module).exists():
            next_submodule = SubModule.objects.get(module=last_module).order_by('order').first()
            if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
                next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
                output = {
                    'type': 'content',
                    "module": last_module.order,
                    "submodule": next_submodule.order,
                    "content": next_content.order,
                    "course": course.code,
                }
            else:
                output = {
                    'type': 'done',
                    "course": course.code,
                }
        else:
            """get next module"""
            output = {
                'type': 'module_done',
                "module_pk": last_module.pk
            }
    else:
        """get next module"""
        output = {
            'type': 'module_done',
            "module_pk": last_module.pk
        }

    print("Output is ", output)
    if last_progress:
        last_progress.save()
    return output


def load_course_content(ussd_request):
    print("trying to get content")
    course_progress_fetched = ussd_request.session.get('course_progress_fetched')
    print("the course progress fetched is ", course_progress_fetched)
    order_id = int(course_progress_fetched['content'])
    submodule_id = int(course_progress_fetched['submodule'])
    module_id = int(course_progress_fetched['module'])
    course_code = course_progress_fetched['course']
    print("Order ID ", order_id)
    print("Submodule ID ", submodule_id)
    print("Module ID ", module_id)
    print("Course Code ID ", course_code)
    content = ModuleContent.objects.get(order=order_id, submodule__order=submodule_id, submodule__module__order=module_id, submodule__module__course__code=course_code)
    print("completed get content")
    return content


def get_course_topics(ussd_request):
    course = Course.objects.get(code=ussd_request.session.get('course_to_learn'))
    return course.module_set.all()


def get_course_subtopics(ussd_request):
    topic = Module.objects.get(pk=int(ussd_request.session.get('course_topic_to_learn')))
    return topic.submodule_set.all()


def get_course_progress_before_begin(ussd_request):
    course_subtopic_to_learn = ussd_request.session.get('course_subtopic_to_learn')
    next_submodule = SubModule.objects.get(pk=int(course_subtopic_to_learn))
    if ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first():
        next_content = ModuleContent.objects.filter(submodule=next_submodule).order_by('order').first()
        output = {
            'type': 'content',
            "module": next_submodule.module.order,
            "submodule": next_submodule.order,
            "content": next_content.order,
            "course": next_submodule.module.course.code,
        }
    else:
        next_content = None
        output = {
            'type': 'done',
            "course": ussd_request.session.get('course_to_learn'),
        }
    return output

def save_course_learning_progess(ussd_request):
    print("Trying to save the content")
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    course_progress_fetched = ussd_request.session.get('course_progress_fetched')
    print("the course progress fetched is ", course_progress_fetched)
    order_id = int(course_progress_fetched['content'])
    submodule_id = int(course_progress_fetched['submodule'])
    module_id = int(course_progress_fetched['module'])
    course_code = course_progress_fetched['course']
    content = ModuleContent.objects.get(order=order_id, submodule__order=submodule_id,
                                        submodule__module__order=module_id, submodule__module__course__code=course_code)
    try:
        CourseProgress.objects.get(student=student, course=content.submodule.module.course, module=content.submodule.module,
                                  submodule=content.submodule, module_content=content)
    except:
        CourseProgress.objects.create(student=student, course=content.submodule.module.course,
                                      module=content.submodule.module,
                                      submodule=content.submodule, module_content=content)
    print("saving content data complete")
    return True


def save_quiz_taking_progess(ussd_request):
    print("Trying to save the quiz")
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    course_quiz_progress = ussd_request.session.get('course_quiz_progress')
    print("the course progress fetched is ", course_quiz_progress)
    question = course_quiz_progress['question']
    answer_to_quiz_question = ussd_request.session.get('answer_to_quiz_question')
    print("answer to quiz is ", answer_to_quiz_question)
    QuizProgress.objects.create(student=student, question=question, quiz=question.quiz)
    ans = Answer.objects.get(pk=int(answer_to_quiz_question))
    QuizTakerAnswer.objects.create(student=student, question=question, answer=ans, is_correct=ans.is_correct)
    print("saving quiz data complete")
    return True


def check_if_module_quiz(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    course_progress_fetched = ussd_request.session.get('course_progress_fetched')
    print("the course progress fetched is ", course_progress_fetched)
    module_id = course_progress_fetched['module_pk']
    module = Module.objects.get(pk=int(module_id))
    """ get quiz"""
    try:
        quiz = Quiz.objects.get(module=module)
        output = {
            'type': 'quiz',
            "module": module.order,
            "course": module.course.code,
        }
    except:
        output = {
            'type': 'no_quiz',
        }
    print(output)
    return output


def get_quiz_data(ussd_request):
    print("beginning qqqqqqqqqqqquuuuuuuuuiiiiiiiiizzzzzzzzzz")
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    course_progress_fetched = ussd_request.session.get('course_progress_fetched')
    print("the course progress fetched is ", course_progress_fetched)
    module_id = course_progress_fetched['module']
    course_code = course_progress_fetched['course']
    module = Module.objects.get(order=int(module_id), course__code=course_code)
    """ get quiz"""
    quiz = Quiz.objects.get(module=module)
    print("returning quiz")
    return quiz


def get_course_quiz_progress(ussd_request):
    print("getting course quiz progress")
    quiz_data = ussd_request.session.get('quiz_data')
    print(quiz_data)
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    module_id = quiz_data['module']
    course_code = quiz_data['course']
    module = Module.objects.get(order=int(module_id), course__code=course_code)
    """ get quiz"""
    quiz = Quiz.objects.get(module=module)
    last_data = QuizProgress.objects.filter(student=student, quiz=quiz_data).order_by('created').last()
    if last_data:
        last_question = last_data.question
        if Question.objects.filter(quiz=quiz_data, order=last_question.order + 1).exists():
            next_question = Question.objects.get(quiz=quiz_data, order=last_question.order + 1)
            if Question.objects.filter(quiz=quiz_data, order=next_question.order + 1).exists():
                has_next_too = True
            else:
                has_next_too = False
            output = {
                'type':'question',
                'question':next_question,
                "has_next":has_next_too
            }
        else:
            output = {
                'type': 'end',
                "has_next": False
            }
    else:
        if Question.objects.filter(quiz=quiz_data).exists():
            next_question = Question.objects.filter(quiz=quiz_data).first()
            if Question.objects.filter(quiz=quiz_data, order=next_question.order + 1).exists():
                has_next_too = True
            else:
                has_next_too = False
            output = {
                'type':'question',
                'question':next_question,
                "has_next":has_next_too
            }
        else:
            output = {
                'type': 'end',
                "has_next": False
            }
    print(output)
    return output



###### TRIVIA QUIZ ############
def get_trivia_quiz_list(ussd_request):
    d =  TriviaQuiz.objects.all()
    k = []
    for i in d:
        if i.questions.count() > 0:
            k.append(i)
    return k

def get_trivia_quiz_list_for_bank(ussd_request):
    d =  TriviaQuiz.objects.all()
    return d


def get_trivia_bank_data(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    trivia_quiz_to_take = ussd_request.session.get('trivia_bank_to_take')
    print("the trivia bank to take is ", trivia_quiz_to_take)
    quiz = TriviaQuiz.objects.get(pk=int(trivia_quiz_to_take))
    progress = TriviaBankProgress.objects.filter(student=student, trivia=quiz).last()
    if progress:
        bank = progress.bank
        if TriviaBank.objects.filter(category=quiz, order=bank.order + 1).exists():
            next_bank_d = TriviaBank.objects.get(category=quiz, order=bank.order + 1)
            if TriviaBank.objects.filter(category=quiz, order=next_bank_d.order + 1).exists():
                has_next_too = True
            else:
                has_next_too = False
            output = {
                'type': 'bank',
                'bank': next_bank_d,
                "has_next": has_next_too
            }
        else:
            output = {
                'type': 'end',
                "has_next": False
            }
            TriviaBankProgress.objects.filter(student=student, trivia=quiz).delete()
    else:
        if TriviaBank.objects.filter(category=quiz).exists():
            next_bank_d = TriviaBank.objects.filter(category=quiz).first()
            if TriviaBank.objects.filter(category=quiz, order=next_bank_d.order + 1).exists():
                has_next_too = True
            else:
                has_next_too = False
            output = {
                'type': 'bank',
                'bank': next_bank_d,
                "has_next": has_next_too
            }
        else:
            output = {
                'type': 'no_data',
                "has_next": False
            }

    print(output)
    return output


def save_trivia_bank_data_progress(ussd_request):
    print("Trying to save the quiz")
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    bank_data = ussd_request.session.get('trivia_bank_data')
    print("-----------------------")
    print(bank_data)
    trivia_quiz_to_take = ussd_request.session.get('trivia_bank_to_take')
    print("the trivia bank to take is ", trivia_quiz_to_take)
    quiz = TriviaQuiz.objects.get(pk=int(trivia_quiz_to_take))
    TriviaBankProgress.objects.create(student=student, trivia=quiz, bank=bank_data['bank'])

    print("saving trivia bank data complete")
    return True



def get_trivia_quiz_data(ussd_request):
    trivia_quiz_to_take = ussd_request.session.get('trivia_quiz_to_take')
    print("the trivia quiz to take is ", trivia_quiz_to_take)
    quiz = TriviaQuiz.objects.get(pk=int(trivia_quiz_to_take))
    return quiz


def get_trivia_quiz_progress(ussd_request):
    quiz_data = ussd_request.session.get('trivia_quiz_data')
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    last_data = TriviaQuizProgress.objects.filter(student=student, quiz=quiz_data, is_completed=False).order_by('created').last()
    if last_data:
        last_question = last_data.question
        last_order = last_question.order
        q_ids = last_data.question_ids
        last_index = q_ids.index(last_order)
        print("last_indexn ", last_index)
        print("q_ids ", q_ids)
        try:
            next_order_id = q_ids[last_index + 1]
            next_question = TriviaQuestion.objects.get(quiz=quiz_data, order=next_order_id)
            output = {
                'type': 'question',
                'question': next_question,
                "has_next": False,
                "question_list": last_data.question_ids,
            }
            try:
                v = q_ids[last_index + 2]
                output.update({'has_next':True})
            except:
                pass
        except:
            last_data.is_completed = True
            last_data.save()
            TriviaQuizProgress.objects.filter(student=student, quiz=quiz_data).delete()
            TriviaQuizTakerAnswer.objects.filter(student=student, question__quiz=quiz_data).delete()
            output = {
                'type': 'end',
                "has_next": False
            }
    else:
        if TriviaQuestion.objects.filter(quiz=quiz_data).exists():
            """Get random quiz questions"""
            trivia_quiz_questions_size = get_clobal_config().trivia_quiz_questions
            if TriviaQuestion.objects.filter(quiz=quiz_data).count() > 0:
                dx = TriviaQuestion.objects.filter(quiz=quiz_data).values_list('order', flat=True)
                q_ids = []
                for i in dx:
                    q_ids.append(i)
                print("q ids ", q_ids)
                q_ids_list = random.sample(q_ids, min(len(q_ids), trivia_quiz_questions_size))
                print("q id list, ", q_ids_list)
                next_question = TriviaQuestion.objects.filter(quiz=quiz_data, order=int(q_ids_list[0])).first()
                if TriviaQuestion.objects.filter(quiz=quiz_data, order=next_question.order + 1).exists():
                    has_next_too = True
                else:
                    has_next_too = False
                output = {
                    'type':'question',
                    'question':next_question,
                    "has_next":has_next_too,
                    "question_list":q_ids_list,
                }
            else:
                output = {
                    'type': 'no_data',
                    "has_next": False
                }
        else:
            output = {
                'type': 'no_data',
                "has_next": False
            }
    print(output)
    return output


def save_trivia_quiz_taking_progress(ussd_request):
    print("Trying to save the trivia quiz")
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    trivia_quiz_progress = ussd_request.session.get('trivia_quiz_progress')
    print("the course progress fetched is ", trivia_quiz_progress)
    question = trivia_quiz_progress['question']
    answer_to_quiz_question = ussd_request.session.get('answer_to_trivia_quiz_question')
    print("answer to quiz is ", answer_to_quiz_question)
    triv = TriviaQuizProgress.objects.create(student=student, question=question, quiz=question.quiz)
    if 'question_list' in trivia_quiz_progress:
        triv.question_ids = trivia_quiz_progress['question_list']
        triv.save()
    ans = TriviaAnswer.objects.get(pk=int(answer_to_quiz_question))
    TriviaQuizTakerAnswer.objects.create(student=student, question=question, answer=ans, is_correct=ans.is_correct)
    print("saving quiz data complete")
    if not trivia_quiz_progress['has_next']:
        ''' calculate and safe result'''
        quiz = question.quiz
        total_questions = len(triv.question_ids)
        correct_ans = TriviaQuizTakerAnswer.objects.filter(student=student, question__quiz=quiz, is_correct=True).count()
        result = TriviaQuizResult.objects.create(student=student, quiz=quiz, total_questions=total_questions, correct_answers=correct_ans)
        return result
    return True

def get_trivia_quiz_result_obj(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    quiz = ussd_request.session.get('trivia_quiz_data')
    """ get last result"""
    result = TriviaQuizResult.objects.filter(student=student, quiz=quiz).order_by('created').last()
    print("result is ", result)
    return result


def take_trivia_quiz_again(ussd_request):
    """Delete all answers """
    quiz_data = ussd_request.session.get('trivia_quiz_data')
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    TriviaQuizProgress.objects.filter(student=student, quiz=quiz_data).delete()
    TriviaQuizTakerAnswer.objects.filter(student=student, question__quiz=quiz_data).delete()
    return True


def check_trivia_quiz_topic_result_method(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    topic_id = ussd_request.session.get('trivia_quiz_topic_to_check_result')
    quiz = TriviaQuiz.objects.get(pk=int(topic_id))
    """ get last result"""
    result = TriviaQuizResult.objects.filter(student=student, quiz=quiz).order_by('created').last()
    print("result quiz is ", result)
    if result:
        return result
    else:
        return "no_result"




################ WORD SEARCH ######################

def get_word_search_result(ussd_request):
    word_to_search = ussd_request.session.get('word_to_search')
    try:
        result = search_word(word_to_search)
        return result
    except:
        return "Operation can't be performed this time. Please try again later"




################ UTME, WAEC, POST-UTME #########################
PAST_QUESTION_TYPES = [
    {'code':'utme', 'title':'UTME'},
    {'code':'wassce', 'title':'WASSCE'},
    {'code':'post-utme', 'title':'POST UTME'},
]
PAST_QUESTION_SUBJECT_CHOICES = [{"code": "english", "title": "English language"}, {"code": "mathematics", "title": "Mathematics"},
         {"code": "commerce", "title": "Commerce"}, {"code": "accounting", "title": "Accounting"},
         {"code": "biology", "title": "Biology"}, {"code": "physics", "title": "Physics"},
         {"code": "chemistry", "title": "Chemistry"}, {"code": "englishlit", "title": "English literature"},
         {"code": "government", "title": "Government"}, {"code": "crk", "title": "Christain Religious Knowledge"},
         {"code": "geography", "title": "Geography"}, {"code": "economics", "title": "Economics"},
         {"code": "irk", "title": "Islamic Religious Knowledge"}, {"code": "civiledu", "title": "Civic Education"},
         {"code": "insurance", "title": "Insurance"}, {"code": "currentaffairs", "title": "Current Affairs"},
         {"code": "history", "title": "History"}]
PAST_QUESTION_YEARS = [2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013]


def get_exam_types(ussd_request):
    return PAST_QUESTION_TYPES


def get_exam_subjects(ussd_request):
    return PAST_QUESTION_SUBJECT_CHOICES


def get_exam_years(ussd_request):
    return PAST_QUESTION_YEARS

def check_uncompleted_prep_quiz(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    chosen_exam_prep_type = ussd_request.session.get('chosen_exam_prep_type')
    chosen_exam_prep_subject = ussd_request.session.get('chosen_exam_prep_subject')
    chosen_exam_prep_year = ussd_request.session.get('chosen_exam_prep_year')

    """ check if uncompleted prep quiz exists"""
    try:
        prep_progress = PrepExamProgress.objects.get(student=student, type=chosen_exam_prep_type, subject=chosen_exam_prep_subject,
                                                 year=chosen_exam_prep_year, is_completed=False)
        return prep_progress.pk
    except:
        return "False"


def discard_uncompleted_prep_quiz(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    chosen_exam_prep_type = ussd_request.session.get('chosen_exam_prep_type')
    chosen_exam_prep_subject = ussd_request.session.get('chosen_exam_prep_subject')
    chosen_exam_prep_year = ussd_request.session.get('chosen_exam_prep_year')
    PrepExamProgress.objects.filter(student=student, type=chosen_exam_prep_type, subject=chosen_exam_prep_subject,
                                    year=chosen_exam_prep_year).update(is_completed=True)
    return True


def get_questions_for_exam(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    chosen_exam_prep_type = ussd_request.session.get('chosen_exam_prep_type')
    chosen_exam_prep_subject = ussd_request.session.get('chosen_exam_prep_subject')
    chosen_exam_prep_year = ussd_request.session.get('chosen_exam_prep_year')
    print("Exam type ", chosen_exam_prep_type)
    print("Exam subject ", chosen_exam_prep_subject)
    print("Exam year ", chosen_exam_prep_year)
    url = "https://questions.aloc.ng/api/q/%s?subject=%s&year=%s&type=%s" % (get_clobal_config().prep_exam_questions,chosen_exam_prep_subject,
                                                                             chosen_exam_prep_year, chosen_exam_prep_type)
    print(url)
    request = requests.get(url)
    dtext = request.text

    print(request.encoding)
    try:
        resp = request.json(encoding="utf8")
    except:
        return {
                'type': 'no_data',
                "has_next": False
            }
    if resp['status'] == 200:
        data = resp['data']
        # print('Gotten data from the api call is ', data)
        ded = TextUtil()
        data = ded.decode_nested(data)
        # print("decoded data is ", data)
        current_qid = 0
        """ create the question progress and flow"""
        PrepExamProgress.objects.filter(student=student, type=chosen_exam_prep_type, subject=chosen_exam_prep_subject,
                                        year=chosen_exam_prep_year).update(is_completed=True)
        prep_progress = PrepExamProgress.objects.create(student=student, type=chosen_exam_prep_type, subject=chosen_exam_prep_subject,
                                                     year=chosen_exam_prep_year, questions=data, current_qid=current_qid)
        prep_progress.save()
        return get_prep_next_question(ussd_request)


def get_prep_next_question(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    chosen_exam_prep_type = ussd_request.session.get('chosen_exam_prep_type')
    chosen_exam_prep_subject = ussd_request.session.get('chosen_exam_prep_subject')
    chosen_exam_prep_year = ussd_request.session.get('chosen_exam_prep_year')
    prep_progress = PrepExamProgress.objects.get(student=student, type=chosen_exam_prep_type,
                                                    subject=chosen_exam_prep_subject,
                                                    year=chosen_exam_prep_year, is_completed=False)
    if prep_progress.current_qid == 90002:
        q = prep_progress.questions[0]
        prep_progress.current_qid = 0
        prep_progress.save()
    else:
        try:
            q = prep_progress.questions[prep_progress.current_qid]
            prep_progress.current_qid += 1
            prep_progress.save()
        except:
            """End"""
            q = None
            prep_progress.is_completed = True
            prep_progress.save()

    if q:
        return {'q':q, 'index':prep_progress.current_qid}
    else:
        return {
                'type': 'end',
                "has_next": False
            }
        # m = nexto(item for item in prep_progress.questions if item["id"] == 49)


def save_prep_exam_quiz_progess(ussd_request):
    print("Trying to save the prep quiz")
    phone = ussd_request.session.get('phone_number')
    answer_to_prep_exam_question = ussd_request.session.get('answer_to_prep_exam_question')
    print("answer chosen is ", answer_to_prep_exam_question)
    student = get_student(phone)
    exam_prep_question_progress = ussd_request.session.get('exam_prep_question_progress')
    print("the course progress fetched is ", exam_prep_question_progress)
    index = exam_prep_question_progress['index']
    print("Th index is ", index)
    chosen_exam_prep_type = ussd_request.session.get('chosen_exam_prep_type')
    chosen_exam_prep_subject = ussd_request.session.get('chosen_exam_prep_subject')
    chosen_exam_prep_year = ussd_request.session.get('chosen_exam_prep_year')
    prep_progress = PrepExamProgress.objects.get(student=student, type=chosen_exam_prep_type,
                                                 subject=chosen_exam_prep_subject,
                                                 year=chosen_exam_prep_year, is_completed=False)
    if exam_prep_question_progress['q']['answer'] == answer_to_prep_exam_question:
        prep_progress.correct_answers += 1
        prep_progress.save()
        is_correct = "True"
    else:
        is_correct = "False"
    c_ans = exam_prep_question_progress['q']['answer']
    d = {
        "correct_option":str(c_ans).upper(),
        "user_answer":str(answer_to_prep_exam_question).upper(),
        "is_correct":is_correct,
        "correct_option_text": exam_prep_question_progress['q']['option'][c_ans]
    }
    return d


def get_prep_exam_quiz_result_obj(ussd_request):
    print("Entering get result obj")
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    chosen_exam_prep_type = ussd_request.session.get('chosen_exam_prep_type')
    chosen_exam_prep_subject = ussd_request.session.get('chosen_exam_prep_subject')
    chosen_exam_prep_year = ussd_request.session.get('chosen_exam_prep_year')
    prep_progress = PrepExamProgress.objects.filter(student=student, type=chosen_exam_prep_type,
                                                 subject=chosen_exam_prep_subject,
                                                 year=chosen_exam_prep_year, is_completed=True).order_by('created').last()
    tex = "Your Exam Prep Quiz Result \n"
    "Total Questions: {{ prep_exam_quiz_result.total_questions }} \nCorrect Answer: {{ prep_exam_quiz_result.correct_answers }} \n"
    return prep_progress



########### ASK A Teacher #############
def send_question_to_teacher_d(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    subj = ussd_request.session.get('course_to_ask')
    question = ussd_request.session.get('user_question')
    print("question is ",question)
    print("subject is ",subj)
    subject = Course.objects.get(code=subj)
    if subject.supervisor:
        teacher = subject.supervisor
    else:
        teacher = get_super_teacher()
    req_id = generate_digits(10)
    today = datetime.now()
    last_five_minutes = (today + timedelta(minutes=5))
    if not StudentQuestion.objects.filter(question=question, created__gte=last_five_minutes).exists():
        print("creating student question")
        d = StudentQuestion.objects.create(student=student, question=question, assignee=teacher, request_id=req_id)
        output = {
            'type': 'done'
        }
        comp_msg = "Hi %s, \nYou have a new question from your student[%s]:\nKID:%s\nQuestion: %s" % (teacher.fullname, student.fullname, req_id,question)
        send_sms(teacher.phone, comp_msg)
    else:
        output = {
            'type':'same_question'
        }
    return output


def get_stud_answered_q(ussd_request):
    phone = ussd_request.session.get('phone_number')
    student = get_student(phone)
    q = StudentQuestion.objects.filter(student=student, is_answered=True)
    tex = "My answered questions:\n"
    count = 1
    for i in q:
        tex +="%s. %s\n" % (count, i.question)
        count +=1
    return q


def get_answer_to_q_s_d(ussd_request):
    phone = ussd_request.session.get('phone_number')
    q_id = ussd_request.session.get('my_ans_q_selected')
    student = get_student(phone)
    q = StudentQuestion.objects.get(student=student, pk=int(q_id))
    return q


def get_supervisor_questions(ussd_request):
    phone = ussd_request.session.get('phone_number')
    answering_progress = ussd_request.session.get('my_questions_to_answer')
    print("Answering progress is ",answering_progress)
    last_answered = ussd_request.session.get('last_answered')
    print(last_answered)
    if last_answered:
        questions = answering_progress['questions']
        quids = answering_progress['quids']
        print("Gottent quids", quids)

        try:
            next_index = quids.index(last_answered) + 1
            next_pk = quids[next_index]
            current_question = questions.get(pk=int(next_pk))
            output = {
                "type": "question",
                "questions": questions,
                "quids": quids,
                "current_question": current_question
            }
        except:
            output = {
                "type": "done",
            }
    else:
        assignee = Supervisor.objects.get(phone=phone)
        questions = StudentQuestion.objects.filter(assignee=assignee, is_answered=False).order_by('-created')
        quids = list(set([x.pk for x in questions]))
        if len(quids) > 0:
            output = {
                "type":"question",
                "questions":questions,
                "quids":quids,
                "current_question":questions.get(pk=int(quids[0])),
            }
        else:
            output = {
                "type": "done",
            }
    print(output)
    return output


def save_question_answer(ussd_request):
    print("Saving progressss sssss")
    print("Saving progressss sssss")
    print("Saving progressss sssss")
    answering_progress = ussd_request.session.get('my_questions_to_answer')
    last_answered = ussd_request.session.get('last_answered')
    question_answer = ussd_request.session.get('question_answer')
    quids = list(answering_progress['quids'])
    print(quids)
    print(last_answered)
    print(type(quids))
    print(type(last_answered))
    if last_answered:
        this_pk = quids[quids.index(last_answered) + 1]
        # this_pk = quids.index(int(last_answered))
        print("this pk is ", this_pk)
        qs = StudentQuestion.objects.get(pk=this_pk)
    else:
        qs = StudentQuestion.objects.get(pk=int(quids[0]))
    qs.assignee_response = question_answer
    qs.is_answered = True
    qs.save()
    comp_msg = "Hi %s, \nYour teacher has replied to your question:\n%s\n\nAnswer: %s" % (
    qs.student.fullname, qs.question, question_answer)
    send_sms(qs.student.phone, comp_msg)
    return qs.pk
