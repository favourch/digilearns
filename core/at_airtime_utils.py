# works with both python 2 and 3
from __future__ import print_function

import africastalking


class AIRTIME:
    def __init__(self):
        # Set your app credentials
        self.username = "YOUR_USERNAME"
        self.api_key = "YOUR_API_KEY"

        # Initialize the SDK
        africastalking.initialize(self.username, self.api_key)

        # Get the airtime service
        self.airtime = africastalking.Airtime

    def send(self):
        # Set phone_number in international format
        phone_number = 'MyPhoneNumber'

        # Set The 3-Letter ISO currency code and the amount
        amount = "908"
        currency_code = "KES"

    try:
        # That's it hit send and we'll take care of the rest
        responses = self.airtime.send(phone_number=phone_number, amount=amount, currency_code=currency_code)
        print(responses)
    except Exception as e:
        print("Encountered an error while sending airtime:%s" % str(e))


if __name__ == '__main__':
    AIRTIME().send()
