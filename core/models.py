from decimal import Decimal

import jsonfield
from django.core.exceptions import ObjectDoesNotExist
from django.db import models


# Create your models here.


class BaseModel(models.Model):
    """
    Abstract model to be inherited by all other models
    """
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True


class GlobalConfig(models.Model):
    trivia_quiz_questions = models.PositiveIntegerField(default=10)
    prep_exam_questions = models.PositiveIntegerField(default=10)

    def __str__(self):
        return "Config set"

class State(models.Model):
    name = models.CharField(max_length=40, unique=True)
    capital = models.CharField(max_length=40)
    latitude = models.CharField(max_length=40, blank=True)
    longitude = models.CharField(max_length=40, blank=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Language(BaseModel):
    """
    Language selection for the users
    """
    code = models.CharField(max_length=40)
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.code


class Classlevel(BaseModel):
    '''
    This modes defines the different classes offered by the app
    '''
    code = models.CharField(max_length=20, unique=True)
    name = models.CharField(max_length=20, null=True)

    def __str__(self):
        return self.name


class OrderField(models.PositiveIntegerField):

    def __init__(self, for_fields=None, *args, **kwargs):

        self.for_fields = for_fields
        super(OrderField, self).__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):

        if getattr(model_instance, self.attname) is None:
            # no current value
            try:
                qs = self.model.objects.all()
                if self.for_fields:
                    # filter by objects with the same field values
                    # for the fields in "for_fields"
                    query = {field: getattr(model_instance, field) for
                             field in self.for_fields}
                    qs = qs.filter(**query)
                # get the order of the last item
                last_item = qs.latest(self.attname)
                value = last_item.order + 1
            except ObjectDoesNotExist:
                value = 0
            setattr(model_instance, self.attname, value)
            return value
        else:
            return super(OrderField, self).pre_save(model_instance, add)


class Supervisor(BaseModel):
    '''
    This model defines supervisor or teacher on the platform
    '''
    phone = models.CharField(max_length=15, unique=True)
    fullname = models.TextField()
    # token = models.CharField(max_length=10, default="0123456789")

    def __str__(self):
        return self.fullname


class Course(BaseModel):
    code = models.CharField(max_length=20, unique=True)
    title = models.CharField(max_length=120)
    class_level = models.ForeignKey(Classlevel, on_delete=models.CASCADE, related_name="learning_course_klass")
    supervisor = models.ForeignKey(Supervisor, on_delete=models.CASCADE, related_name="supervisor_for_course", null=True)

    def __str__(self):
        return "%s - %s" % (self.code,self.title)


class Module(BaseModel):
    title = models.CharField(max_length=2000)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    order = OrderField(blank=True, for_fields=['course'])

    class Meta:
        ordering = ['order']

    def __str__(self):
        return '{}. {}'.format(self.order, self.title)


class SubModule(BaseModel):
    title = models.CharField(max_length=2000)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    order = OrderField(blank=True, for_fields=['module'])

    class Meta:
        ordering = ['order']

    def __str__(self):
        return '{}. {}'.format(self.order, self.title)


class ModuleContent(BaseModel):
    submodule = models.ForeignKey(SubModule, on_delete=models.CASCADE)
    content = models.TextField()
    order = OrderField(blank=True, for_fields=['submodule'])

    class Meta:
        ordering = ['order']

    def __str__(self):
        return '%s - %s - %s' % (self.order, self.submodule.title, self.content)



class Student(BaseModel):
    '''
    This model defines the properties of a student and also inherits from the User model
    '''
    phone = models.CharField(max_length=15, unique=True)
    first_name = models.CharField(max_length=120)
    last_name = models.CharField(max_length=120)
    dob = models.DateTimeField(null=True)
    parent_phone = models.CharField(max_length=15, null=True, blank=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=True)
    current_class = models.ForeignKey(Classlevel, on_delete=models.CASCADE, null=True)
    teacher = models.ForeignKey(Supervisor, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.fullname

    @property
    def fullname(self):
        return "%s %s" % (self.first_name, self.last_name)

    @property
    def courses(self):
        return Course.objects.filter(class_level=self.current_class)

    @property
    def state_name(self):
        return self.state.name


class StudentConfiguration(BaseModel):
    phone = models.CharField(max_length=20, unique=True)
    default_language = models.ForeignKey(Language, on_delete=models.CASCADE, null=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.phone


log_type_choices = (
    ('content','content'),
    ('quiz','quiz'),
    ('exam','exam'),
)


class CourseProgress(BaseModel):
    # log_type = models.CharField(max_length=20, choices=log_type_choices)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    submodule = models.ForeignKey(SubModule, on_delete=models.CASCADE)
    module_content = models.ForeignKey(ModuleContent, on_delete=models.CASCADE)
    is_module_completed = models.BooleanField(default=False)
    is_submodule_completed = models.BooleanField(default=False)
    is_quiz_completed = models.BooleanField(default=False)

    class Meta:
        unique_together = (('student','course','module','submodule','module_content'),)

    def __str__(self):
        return self.student.fullname


class Quiz(BaseModel):
    """
    Quiz , Exam or Test
    """
    module = models.ForeignKey(Module, on_delete=models.CASCADE, null=True) # May have topic or may not have
    name = models.CharField(max_length=1000)
    question_count = models.IntegerField(default=0)
    is_roll_out = models.BooleanField(default=False)

    class Meta:
        ordering = ['created', ]
        verbose_name_plural ="Quizzes"

    def __str__(self):
        return self.name

    @property
    def questions(self):
        return self.question_set.all()


class Question(BaseModel):
    """
    Each question object per Quiz. Question is meant to have options and choices
    """
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    label = models.CharField(max_length=1000)
    order = OrderField(blank=True, for_fields=['quiz'])

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.label

    @property
    def options(self):
        return self.answer_set.all()


class Answer(BaseModel):
    """
    This is for question answer choices. anad to mark those correct choice or choices
    """
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    text = models.CharField(max_length=1000)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.question.label, self.text)


class QuizTaker(BaseModel):
    """
    This is to track student who attempted a quiz and the answers they chose for each question
    """
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    correct_answers = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.student.fullname


class QuizTakerAnswer(BaseModel):
    """
    This is to track of what student chose in the quiz
    """
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s - %s" % (self.student.fullname, self.question, self.answer)


class QuizProgress(BaseModel):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    class Meta:
        unique_together = (('student','quiz','question'),)

    def __str__(self):
        return self.student.fullname


class UserSession(BaseModel):
    """
    this class facilitates the creation of ussd interaction objects. Storing user sessions
    """
    session_id = models.CharField(max_length=100)
    phone = models.BigIntegerField()
    screens = jsonfield.JSONField()

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return u"%s %s %s" %  (self.session_id, self.phone, self.screens)


################## TRIVIA QUIZ ################################

class TriviaQuiz(BaseModel):
    """
    Trivia Quiz
    """
    name = models.CharField(max_length=1000)

    class Meta:
        ordering = ['created', ]
        verbose_name_plural ="Trivia Quizzes"

    def __str__(self):
        return self.name

    @property
    def questions(self):
        return self.triviaquestion_set.all()


class TriviaBank(BaseModel):
    """
    Trivia bank fr each trivia category
    """
    category = models.ForeignKey(TriviaQuiz, on_delete=models.CASCADE)
    content = models.TextField()
    order = OrderField(blank=True, for_fields=['category'])

    class Meta:
        ordering = ['order']

    def __str__(self):
        return "%s - %s" % (self.category,self.content)

    @property
    def prep_content(self):
        return str("%s /\n\r\n" % self.content)



class TriviaBankProgress(BaseModel):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    trivia = models.ForeignKey(TriviaQuiz, on_delete=models.CASCADE)
    bank = models.ForeignKey(TriviaBank, on_delete=models.CASCADE)

    class Meta:
        unique_together = (('student','trivia','bank'),)

    def __str__(self):
        return self.student.fullname


class TriviaQuestion(BaseModel):
    """
    Each question object per Quiz. Question is meant to have options and choices
    """
    quiz = models.ForeignKey(TriviaQuiz, on_delete=models.CASCADE)
    label = models.CharField(max_length=1000)
    order = OrderField(blank=True, for_fields=['quiz'])

    class Meta:
        ordering = ['order']

    def __str__(self):
        return self.label

    @property
    def options(self):
        return self.triviaanswer_set.all()


class TriviaAnswer(BaseModel):
    """
    This is for question answer choices. anad to mark those correct choice or choices
    """
    question = models.ForeignKey(TriviaQuestion, on_delete=models.CASCADE)
    text = models.CharField(max_length=1000)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.question.label, self.text)


class TriviaQuizResult(BaseModel):
    """
    This is to track student who attempted a trivia quiz and their result
    """
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    quiz = models.ForeignKey(TriviaQuiz, on_delete=models.CASCADE)
    correct_answers = models.IntegerField(default=0)
    total_questions = models.IntegerField(default=0)

    def __str__(self):
        return self.student.fullname

    @property
    def percentage_score(self):
        try:
            x = str(round(Decimal(self.correct_answers / self.total_questions) * 100))
            return x+"%"
        except:
            return "0.00%"


class TriviaQuizTakerAnswer(BaseModel):
    """
    This is to track of what student chose in the trivia quiz
    """
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    question = models.ForeignKey(TriviaQuestion, on_delete=models.CASCADE)
    answer = models.ForeignKey(TriviaAnswer, on_delete=models.CASCADE)
    is_correct = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s - %s" % (self.student.fullname, self.question, self.answer)


class TriviaQuizProgress(BaseModel):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    quiz = models.ForeignKey(TriviaQuiz, on_delete=models.CASCADE)
    question = models.ForeignKey(TriviaQuestion, on_delete=models.CASCADE)
    question_ids =jsonfield.JSONField()
    is_completed = models.BooleanField(default=False)

    class Meta:
        unique_together = (('student','quiz','question'),)

    def __str__(self):
        return self.student.fullname


################# PREP EXAM #########################
class PrepExamProgress(BaseModel):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    year = models.CharField(max_length=20)
    subject = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    questions =jsonfield.JSONField()
    current_qid =models.PositiveIntegerField(default=90002)
    is_completed = models.BooleanField(default=False)
    correct_answers = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.student.fullname

    @property
    def total_questions(self):
        return len(self.questions)

    @property
    def percentage_score(self):
        try:
            x = str(round(Decimal(self.correct_answers / self.total_questions) * 100))
            return x + "%"
        except:
            return "0.00%"

############################ Stuzdent-Teacher ################################
class StudentQuestion(BaseModel):
    request_id = models.CharField(max_length=10)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    question = models.CharField(max_length=400)
    is_answered = models.BooleanField(default=False)
    assignee = models.ForeignKey(Supervisor, on_delete=models.CASCADE)
    assignee_response = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.question
