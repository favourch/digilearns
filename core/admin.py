from django.contrib import admin

# Register your models here.
from core.models import UserSession, Student, Language, StudentConfiguration, Classlevel, State, Course, Module, \
    ModuleContent, CourseProgress, SubModule, Quiz, Question, Answer, QuizTaker, QuizTakerAnswer, QuizProgress, \
    TriviaQuiz, TriviaQuizResult, TriviaQuizTakerAnswer, TriviaQuizProgress, TriviaQuestion, TriviaAnswer, GlobalConfig, \
    PrepExamProgress, StudentQuestion, Supervisor, TriviaBank


class PrepExamProgressAdmin(admin.ModelAdmin):
    list_display = ['student', 'year', 'subject', 'type', 'current_qid', 'is_completed', 'correct_answers', 'questions', 'created']
    search_fields = ['student__phone']
    list_filter = ['year','subject', 'type', 'is_completed']


class StudentQuestionAdmin(admin.ModelAdmin):
    list_display = ['student', 'question', 'assignee', 'is_answered','created']
    search_fields = ['student__phone', 'supervisor__phone']
    list_filter = ['is_answered']


admin.site.register(UserSession)
admin.site.register(StudentQuestion, StudentQuestionAdmin)
admin.site.register(Supervisor)
admin.site.register(GlobalConfig)
admin.site.register(Student)
admin.site.register(Language)
admin.site.register(StudentConfiguration)
admin.site.register(Classlevel)
admin.site.register(State)
admin.site.register(Course)
admin.site.register(ModuleContent)
admin.site.register(Module)
admin.site.register(SubModule)
admin.site.register(CourseProgress)
admin.site.register(Quiz)
admin.site.register(Question)
admin.site.register(Answer)
admin.site.register(QuizTaker)
admin.site.register(QuizTakerAnswer)
admin.site.register(QuizProgress)
admin.site.register(TriviaQuiz)
admin.site.register(TriviaQuestion)
admin.site.register(TriviaAnswer)
admin.site.register(TriviaQuizProgress)
admin.site.register(TriviaQuizTakerAnswer)
admin.site.register(TriviaQuizResult)
admin.site.register(PrepExamProgress)
admin.site.register(TriviaBank)