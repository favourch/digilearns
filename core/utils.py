from decimal import Decimal
from random import choice
from string import digits
import re
import requests
from africastalking import SMSService, AirtimeService

from core.models import StudentConfiguration, Student, Supervisor, GlobalConfig
from digilearns.settings import RAPID_API_KEY, SUPER_TEACHER_PHONE, AFRICASTALKING_APPNAME, AFRICASTALKING_KEY, \
    AFRICASTALKING_SENDER_ID, PREP_EXAM_QUESTIONS


def get_student_configuration(phone_number):
    stud_conf, _ = StudentConfiguration.objects.get_or_create(phone=phone_number)
    return stud_conf


def get_student(phone):
    return Student.objects.get(phone=phone)


def search_word(word):
    url = "https://twinword-word-graph-dictionary.p.rapidapi.com/definition/"

    querystring = {"entry": word}

    headers = {
        'x-rapidapi-host': "twinword-word-graph-dictionary.p.rapidapi.com",
        'x-rapidapi-key': RAPID_API_KEY
    }

    response = requests.request("GET", url, headers=headers, params=querystring)
    resp = response.json()
    if resp['result_code'] == "200":
        """look for  meaning"""
        if 'meaning' in resp:
            if 'noun' in resp['meaning']:
                result = str(resp['meaning']['noun'])
                if result != "":
                    k = result.split('\n')
                    f = k[0]
                    return word+"\n"+f
                elif 'verb' in resp['meaning']:
                    result = str(resp['meaning']['verb'])
                    if result != "":
                        k = result.split('\n')
                        f = k[0]
                        return word+"\n"+f
                    elif 'adverb' in resp['meaning']:
                        result = str(resp['meaning']['adverb'])
                        if result != "":
                            k = result.split('\n')
                            f = k[0]
                            return word + "\n" + f
                        elif 'adjective' in resp['meaning']:
                            result = str(resp['meaning']['adjective'])
                            if result != "":
                                k = result.split('\n')
                                f = k[0]
                                return word + "\n" + f
                return "No meaning found"
            else:
                return "No meaning found"
        else:
            return "No meaning found"
    else:
        return "No meaning found"


class TextUtil:
    """

    """
    def decode(self, raw):
        raw = bytes(raw, "ascii", 'ignore')
        return raw.decode('utf-8', 'ignore')

    def decode_nested(self, ob):
        if isinstance(ob, dict):
            for k, v in ob.items():
                if isinstance(v, str) or isinstance(v, int) or isinstance(v, Decimal):
                    ob[k] = self.decode(str(v))
                else:
                    self.decode_nested(v)
        elif isinstance(ob, list):
            for ind, v in enumerate(ob):
                ob[ind] = self.decode_nested(v)
        else:
            ob = self.decode(str(ob))
        return ob


def get_super_teacher():
    try:
        return Supervisor.objects.get(phone=SUPER_TEACHER_PHONE)
    except:
        sp = Supervisor.objects.create(phone=SUPER_TEACHER_PHONE, fullname=SUPER_TEACHER_PHONE)
        return sp


def generate_digits(length):
    code = ""
    for i in range(length):
        code+= choice(digits)
    return code

def send_sms(to, message):
    if isinstance(to, str):
        data = [to]
    else:
        data = to
    receivers = []
    for i in data:
        receivers.append(sendable_phone_number(i))
    print("final receiveers ", receivers)
    service = SMSService(AFRICASTALKING_APPNAME, AFRICASTALKING_KEY)
    print("sending sms")
    try:
        resp = service.send(message, receivers, sender_id=AFRICASTALKING_SENDER_ID)
        print(resp)
        return resp
    except Exception as e:
        print(str(e))
        pass


def sendable_phone_number(phone):
    if isinstance(phone, list):
        final_f = []
        for i in phone:
            i = str(i).strip()
            re.sub('[^0-9+]+', '', i)
            if i:
                value = i
                if str(value).startswith("+"):
                    val = value
                elif str(value).startswith("234"):
                    val = "+%s" % (str(value))
                elif str(value).startswith("0"):
                    val = "+234%s" % (str(value)[1:])
                elif str(value).startswith("+2340"):
                    val = "+234%s" % (str(value)[5:])
                elif str(value).startswith("2340"):
                    val = "+234%s" % (str(value)[4:])
                else:
                    val = "+234%s" % (str(value))
                final_f.append(val)
        return final_f
    else:
        phone = str(phone)
        re.sub('[^0-9+]+', '', phone)
        if phone:
            value = phone
            if str(value).startswith("+"):
                val = value
            elif str(value).startswith("234"):
                val = "+%s" % (str(value))
            elif str(value).startswith("0"):
                val = "+234%s" % (str(value)[1:])
            elif str(value).startswith("+2340"):
                val = "+234%s" % (str(value)[5:])
            elif str(value).startswith("2340"):
                val = "+234%s" % (str(value)[4:])
            else:
                val = "+234%s" % (str(value))
            return val
    return False

def get_clobal_config():
    if GlobalConfig.objects.all().exists():
        c = GlobalConfig.objects.all().first()
    else:
        c = GlobalConfig.objects.create(prep_exam_questions=PREP_EXAM_QUESTIONS)
    return c


def send_airtime(to, amount):
    if isinstance(to, str):
        data = [to]
    else:
        data = to
    receivers = []
    for i in data:
        receivers.append(sendable_phone_number(i))
    currency = 'NGN'
    print(receivers)
    service = AirtimeService(AFRICASTALKING_APPNAME, AFRICASTALKING_KEY)
    recipients = []
    for i in receivers:
        recipients.append({'phoneNumber':i, 'amount':amount, 'currency_code':currency})
    try:
        resp = service.send(amount=amount,currency_code=currency, recipients=recipients)
        # resp = service.send(amount=amount,currency_code=currency, phone_number="+2348165275871")
        print(resp)
        return resp
    except Exception as e:
        print(str(e))
        pass
