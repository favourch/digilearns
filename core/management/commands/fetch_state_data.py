from django.core.management.base import BaseCommand, CommandError
import requests

from core.models import State


class Command(BaseCommand):

    def handle(self, *args, **options):
        BASE_URL = 'http://locationsng-api.herokuapp.com/api/v1/states'
        response = requests.get(BASE_URL)
        data = response.json()
        print(data)
        for i in data:
            state_name = str(i['name'])
            try:
                state = State.objects.get(name=state_name)
            except:
                state = State.objects.create(
                    name=state_name,
                    capital=i['capital'],
                )
                state.save()
            # if state_name != 'Federal Capital Territory':
            #     url = "http://locationsng-api.herokuapp.com/api/v1/states/%s/details" % (state_name.lower().replace(' ','_'))
            #     print(url)
            #     jresponse = requests.get(url)
            #     jdata = jresponse.json()
            #     if 'lgas' in jdata:
            #         for lga in jdata['lgas']:
            #             print("saving lgas for %s" % state_name)
            #             try:
            #                 LGA.objects.get(state=state, name=lga)
            #             except:
            #                 LGA.objects.create(state=state, name=lga)
            #     if 'longitude' in jdata:
            #         longitude = jdata['longitude'],
            #     if 'latitude' in jdata:
            #         latitude = jdata['latitude'],
            #     state.save()
