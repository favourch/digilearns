from django.core.management.base import BaseCommand, CommandError
import requests

from core.models import State
from core.utils import search_word, send_sms, sendable_phone_number


class Command(BaseCommand):

    def handle(self, *args, **options):
        phone = sendable_phone_number("07066568866")
        comp_msg = "Hi %s, \nYou have a new question from your student[%s]:\n%s" % (
        "raheem", "Yusuf", "What is the highest prime number")
        print(phone)
        send_sms(phone,comp_msg)