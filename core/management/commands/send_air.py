from django.core.management.base import BaseCommand, CommandError
import requests

from core.models import State
from core.utils import search_word, send_sms, sendable_phone_number, send_airtime


class Command(BaseCommand):

    def handle(self, *args, **options):
        phone = sendable_phone_number("08165275871")
        amount = 50
        send_airtime(phone,amount)