from django.core.management.base import BaseCommand, CommandError
import requests

from core.models import State
from core.utils import search_word


class Command(BaseCommand):

    def handle(self, *args, **options):
        p = search_word("gone")
        print(p)