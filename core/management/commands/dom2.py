from django.core.management.base import BaseCommand, CommandError
import requests

from core.models import State
from core.utils import search_word, TextUtil


class Command(BaseCommand):

    def handle(self, *args, **options):
        d = {
            "message": "We could not find what you asked for, but got you this",
            "subject": "mathematics",
            "status": 200,
            "data": [
                {
                    "id": 1,
                    "question": "A regular polygon has each of it angles at 160. What is the number of sides of the polygon? ",
                    "option": {
                        "a": "36",
                        "b": "9",
                        "c": "18",
                        "d": "20"
                    },
                    "section": "",
                    "image": "",
                    "answer": "c",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2006"
                },
                {
                    "id": 49,
                    "question": "A sector circle of radius 7.2cm which subtends an angle of 3000 at the centre is used to   form a cone. What is the radius of the base of the cone?. ",
                    "option": {
                        "a": "6cm ",
                        "b": "7cm ",
                        "c": "8cm    ",
                        "d": "9cm      "
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2009"
                },
                {
                    "id": 15,
                    "question": "A bag contains 10 balls of which 3 are red and 7 are white. Two balls are drawn at random. Find the probability of none of the balls is red, if the draw is with replacement: ",
                    "option": {
                        "a": "0.9",
                        "b": "1",
                        "c": "0.47",
                        "d": "0.49"
                    },
                    "section": "",
                    "image": "",
                    "answer": "d",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2006"
                },
                {
                    "id": 14,
                    "question": "The trigonometric expression cos 2A +sin 2A can be written as    ",
                    "option": {
                        "a": "cosA (cosA-sinA),    ",
                        "b": "Cos2A +sin2A-2sinAcosA,  ",
                        "c": "Cos2A + sin2A = Cos2A \u2013sin2A+2sinAcosA",
                        "d": "cos2A+sin2A"
                    },
                    "section": "",
                    "image": "",
                    "answer": "c",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2006"
                },
                {
                    "id": 72,
                    "question": "Solve the equation 3x+1 = 271-x  ",
                    "option": {
                        "a": "1\/2",
                        "b": "-1\/2",
                        "c": "3\/4",
                        "d": "-3\/4"
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2013"
                },
                {
                    "id": 16,
                    "question": "A bag contains 10 balls of which 3 are red and 7 are white. Two balls are drawn at random. Find the probability of none of the balls is red, if the draw is without replacement ",
                    "option": {
                        "a": "0.1",
                        "b": "0.47",
                        "c": "0.42",
                        "d": "0.21"
                    },
                    "section": "",
                    "image": "",
                    "answer": "b",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2006"
                },
                {
                    "id": 43,
                    "question": "The trigonometric expression cos2A + sin 2A can be written as:  ",
                    "option": {
                        "a": "cosA(cosA + sinA)  ",
                        "b": "B.cos2A + sin2A \u2013 2sinAcosA,  ",
                        "c": "2sinAcosA + cos2A + sin2A,  ",
                        "d": "D.cos2A +sinA  \u2013 2sinAcosA"
                    },
                    "section": "",
                    "image": "",
                    "answer": "c",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2008"
                },
                {
                    "id": 2,
                    "question": "A girl walks 30m from a point P on  a bearing of 040 to a point Q. she   then walks 30m on a bearing of 140 to a point R. the bearing of R from P is ",
                    "option": {
                        "a": "90",
                        "b": "50\u00a0",
                        "c": "45",
                        "d": "40"
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2006"
                },
                {
                    "id": 36,
                    "question": "The expression ax2 + bx takes the value 6 when x = 1 and 10 when x = 2. Find its value when  x = 5       ",
                    "option": {
                        "a": "10",
                        "b": "12",
                        "c": "6",
                        "d": ".-10"
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2008"
                },
                {
                    "id": 20,
                    "question": "The volume of a certain sphere is numerically equal to twice its surface area. The diameter of the sphere is: ",
                    "option": {
                        "a": "6",
                        "b": "9",
                        "c": "12",
                        "d": ""
                    },
                    "section": "",
                    "image": "",
                    "answer": "c",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2007"
                },
                {
                    "id": 63,
                    "question": "Find the probability that a number selected at random 40 to 50 is a prime number   ",
                    "option": {
                        "a": "3\/11",
                        "b": "5\/11",
                        "c": "5\/10",
                        "d": "4\/10"
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2009"
                },
                {
                    "id": 42,
                    "question": "If the distance covered by a body in time t seconds is s = t2 - 6t2  - 5t, what is its initial velocity?  ",
                    "option": {
                        "a": "0ms-1    ",
                        "b": "-4 ms-1 ",
                        "c": "(13t2  - 12t + 5) ms-1   ",
                        "d": "5ms-1 "
                    },
                    "section": "",
                    "image": "",
                    "answer": "d",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2008"
                },
                {
                    "id": 60,
                    "question": "Factorize 6x2 \u2013 14x \u2013 12 ",
                    "option": {
                        "a": "2(x + 3)(3x \u2013 2) ",
                        "b": "6(x \u2013 2)(x + 1) ",
                        "c": "2(x \u2013 3)(3x + 2) ",
                        "d": "6(x + 2)(x \u2013 1) "
                    },
                    "section": "",
                    "image": "",
                    "answer": "c",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2009"
                },
                {
                    "id": 32,
                    "question": "Factorize 16(3x +2y)2 -25(a+2b)2  ",
                    "option": {
                        "a": "(12x + 8y -5a-10b)(12x+8y-5a-10b)",
                        "b": "20(3x +2y-a-2b)(3x+2y+a+1-2b) ",
                        "c": "(12x +8y+5a+10b)(12x +8y-5a-10b)",
                        "d": "20(3x +2y+ a+2b)(3x + 2y+a+2b)"
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2008"
                },
                {
                    "id": 17,
                    "question": "The interior angles of a pentagon are: 180o, 118o, 78o, 84o and  x. The value of x is: ",
                    "option": {
                        "a": "80o ",
                        "b": "108o  ",
                        "c": "120o  ",
                        "d": "134o"
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2007"
                },
                {
                    "id": 57,
                    "question": "If 7 and 189 are the first and fourth terms of a geometric progression respectively find the sum of the first three terms of the progression ",
                    "option": {
                        "a": "182",
                        "b": "180",
                        "c": "91",
                        "d": "63"
                    },
                    "section": "",
                    "image": "",
                    "answer": "c",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2009"
                },
                {
                    "id": 54,
                    "question": "Find the probability of selecting a figure which is parallelogram from a square, a rectangle, a rhombus, a kite and a trapezium  ",
                    "option": {
                        "a": "3\/5 ",
                        "b": "2\/5 ",
                        "c": "1\/5 ",
                        "d": "1\/5 "
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2009"
                },
                {
                    "id": 33,
                    "question": "A cone has base radius 4cm and  height 3cm. The area of its curved surface is",
                    "option": {
                        "a": "12pcm2     ",
                        "b": "20pcm2",
                        "c": "24pcm2\u00a0",
                        "d": "15cm2"
                    },
                    "section": "",
                    "image": "",
                    "answer": "b",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2008"
                },
                {
                    "id": 41,
                    "question": "All the 120 pupils in a school learn Yoruba or Igbo or both. Given that 75 learn Yoruba and 60 learn Igbo. How many learn Igbo only?   ",
                    "option": {
                        "a": "45",
                        "b": "30",
                        "c": "15",
                        "d": "60"
                    },
                    "section": "",
                    "image": "",
                    "answer": "a",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2008"
                },
                {
                    "id": 65,
                    "question": "A number of pencils were shared out among peter, Paul and Audu in the ratio 2:3:5 respectively. If Peter got 5, how many were shared? ",
                    "option": {
                        "a": "15",
                        "b": "25",
                        "c": "30",
                        "d": "50"
                    },
                    "section": "",
                    "image": "",
                    "answer": "b",
                    "solution": "",
                    "examtype": "post-utme",
                    "examyear": "2009"
                }
            ]
        }
        # k = "\u00a0".encode('ascii')
        # c = bytes(k).decode("ascii", "strict")
        # print(c)
        ded = TextUtil()
        data = ded.decode_nested(d)
        print(data)
